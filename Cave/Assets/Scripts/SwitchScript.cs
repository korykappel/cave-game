﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwitchScript : MonoBehaviour {

	bool switchPressP1, switchPressP2;
	public GameObject[] flipDoors;
	
	private SpriteRenderer spriteRenderer;

	public Sprite offSwitch;
	public Sprite onSwitch;
	
	bool isSwitchOn = false;

	List<FlipDoorScript> flipDoorScripts = new List<FlipDoorScript>();

	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}

	// Use this for initialization
	void Start () {
		switchPressP1 = switchPressP2 = false;



		for (int i = 0; i < flipDoors.Length; i++)
			flipDoorScripts.Add(flipDoors[i].GetComponent<FlipDoorScript>());
	}
	
	// Update is called once per frame
	void Update () {
		switchPressP1 = Input.GetButtonDown ("P1_SwitchPress");
		//switchPressP2 = Input.GetButtonDown ("P2_SwitchPress");
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
			if(switchPressP1 && ((Network.isServer && col.gameObject.tag == "P1")  || ( Network.isClient && col.gameObject.tag == "P2") ))
			{
				//always flips the state of the switch, i.e., true => false or false => true
				isSwitchOn ^= true;
				if(isSwitchOn)
					spriteRenderer.sprite = onSwitch;
				else
					spriteRenderer.sprite = offSwitch;
				for (int i = 0; i < flipDoors.Length; i++)
					flipDoorScripts[i].flipSwitch();
			}
	}

	public void reset()
	{
		if(Network.isServer)
		{
			//isSwitchOn = false;
			spriteRenderer.sprite = offSwitch;
		}
	}
}
