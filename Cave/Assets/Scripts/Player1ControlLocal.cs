﻿using UnityEngine;
using System.Collections;

public class Player1ControlLocal : ControlScript {

	private FreezeScriptLocal freezeScript;
	
	private RewindScriptLocal rewindScript;
	private bool tryingToRewind;
	private bool p2Grabbed;
	private GameObject p2;
	private Player2ControlLocal p2script;
	
	private Animator p1Animator;
	private bool animMoving;
	private bool animJumping;
	private bool animDirectionRight;

	private bool canJump;

	Sprite pausedImage;
	P1BoxDisplayLocal boxdisplay;

	private float p1Color;
	private SpriteRenderer p1SpriteRenderer;

	private float percentShadowed;
	private bool turningIntoShadows;
	private float shadowficationDuration;

	// Use this for initialization
	void Start () {
		rewindScript = (RewindScriptLocal)FindObjectOfType (typeof(RewindScriptLocal));
		grabbed 	 = false;
		gameStarted  = true;
		onLadder = false;
		voidLayer = LayerMask.NameToLayer ("Void");
		p2script = FindObjectOfType<Player2ControlLocal> ();
		p2 = p2script.gameObject;
		freezeScript = FindObjectOfType<FreezeScriptLocal>();
		//if (Network.isClient) {
//			rewindScript.enabled = false;
//			cameraScript = (ToggleableCamera)FindObjectOfType (typeof(ToggleableCamera));
//			cam = cameraScript.gameObject; 
//			camView = cameraScript.networkView;
//			camView.RPC("initialize", RPCMode.All, null);
//		}
		//if (Network.isServer)
		//	renderer.sortingOrder = 1;
		renderer.sortingOrder = 7;
		
		p1Animator = this.GetComponent<Animator>();
		//initialize animator vars
		animMoving = false;
		animJumping = false;
		animDirectionRight = true;
		p1Animator.SetBool("Moving", animMoving);
		p1Animator.SetBool("Jumping", animJumping);

		canJump = false;


		boxdisplay = (P1BoxDisplayLocal)FindObjectOfType (typeof(P1BoxDisplayLocal));
		pausedImage =  Resources.Load<Sprite>("Images/Paused_small");

		//renderer.material.color = new Color (1, 1, 1, 1);
		//Debug.Log (renderer.material.color);

		p1Color = 0.0f;
		p1SpriteRenderer = this.GetComponent<SpriteRenderer> ();

		percentShadowed = 0.0f;
		turningIntoShadows = false;
		shadowficationDuration = 0.0f;
	}
	
	void disableCollider(Collider2D col) {
		col.gameObject.layer = voidLayer;
	}
	
	void enableCollider(Collider2D col)
	{
		col.gameObject.layer = 0;
	}
	
	void Update()
	{
//		if(!paused && Input.GetButtonDown("P1_Pause")) {
//			paused = true;
//			pausedStuff();
//		}
//		else if(paused)
//		{
//			if(Input.GetButtonDown("P1_Pause")) 
//			{
//				paused = false;
//				boxdisplay.renderer.enabled = false;
//			}
//			return;
//		}

		updateInputs(ref tryingToRewind, ref move, ref vertMove, ref jump, ref tryingToGrab, ref breakOff, ref p2Grabbed);


	}


	void OnCollisionEnter2D(Collision2D col)
	{
		if(col.gameObject.tag.ToString() == "Jumpable")
		{
			canJump = true;
		}
	} 

	void OnCollisionStay2D(Collision2D col)
	{
		if(col.gameObject.tag.ToString() == "Jumpable")
		{
			canJump = true;
		}
	}


	void pausedStuff()
	{
		boxdisplay.setImageLocal (pausedImage);
		boxdisplay.renderer.enabled = true;
	}

	void OnCollisionExit2D(Collision2D col) {
		if (col.gameObject.tag.ToString () == "Jumpable") {
			//reset the variable for whether colliding with object player can jump off of
			canJump = false;
		}
	}
	
	void FixedUpdate () { //FixedUpdate is good for physics stuff -- has nothing to do with time
		if (!(gameStarted))	return;


//		if(paused)
//		{
//			return;
//		}


		if (!turningIntoShadows) {
			p1Color = 1.0f - 0.92f * rewindScript.getPercentFull ();
			p1SpriteRenderer.color = new Vector4 (p1Color, p1Color, p1Color, 1);
		} 
		else {
			p1Color = 1.0f - 0.95f * percentShadowed;
			p1SpriteRenderer.color = new Vector4 (p1Color, p1Color, p1Color, 1);
		}
		if (!freezeScript.getFrozen()) {

			if(rigidbody2D.isKinematic && rewindScript.movementBarNotFull())
				setKinematicLocal (false);

			if (!grabbed) {
				if (!(tryingToRewind && rewindScript.getPowerEnabled())) 
				{	
					if(onLadder) handleLadderMovement();
					else handleStandardMovement();
				}
				
				//Grabbing code
				if (tryingToGrab && !p2Grabbed && close() && !rewindScript.getBarFull()) 
					p2script.grabPlayerLocal();
				if (!tryingToGrab && p2Grabbed)
					p2script.dropPlayerLocal(transform.position);
				
			} else { //grabbed
				
				transform.position = p2.transform.position;
				//rigidbody2D.velocity = p2.rigidbody2D.velocity;
				rigidbody2D.velocity = Vector2.zero;
				
				
				if(breakOff || rewindScript.getBarFull()) {
//					if (breakOff) Debug.Log ("Broken off");
//					if (tryingToRewind) Debug.Log ("Trying to rewind.");
//					if (rewindScript.getBarFull()) Debug.Log ("Rewind Bar full.");
					dropPlayerLocal (p2.transform.position);
				}				
			}
		} else { //frozend
			setKinematicLocal (true);
			if(grabbed) {
				transform.position = p2.transform.position;
				rigidbody2D.velocity = new Vector2(0,0);
			}
		}



		updateAnimation ();
	}
	
	void updateInputs (ref bool rewind, ref float move, ref float vertMove, ref bool jump, ref bool tryingToGrab, ref bool breakOff, ref bool p2Grabbed)
	{
		if(!enabled) return;

		rewind = Input.GetButton("Rewind");
		move = Input.GetAxis ("P1_Horizontal");
		vertMove = Input.GetAxis ("P1_Vertical");
		jump = Input.GetButtonDown ("P1_Jump");
		tryingToGrab = Input.GetButton ("P1_Action");
		breakOff = Input.GetButtonDown ("P1_BreakOff");
		p2Grabbed = p2script.getGrabbed();
	}

	void updateAnimation() {

		int children = transform.childCount;

		if ((tryingToRewind && rewindScript.getPowerEnabled()) || rewindScript.getBarFull() || freezeScript.getFrozen()) {
			p1Animator.enabled=false;
		} 
		else {
			p1Animator.enabled=true;
			if (animDirectionRight) {
				transform.localScale = new Vector3 (1, 1, 1);
				for (int i = 0; i < children; ++i)
					transform.GetChild(i).transform.localScale = new Vector3(2,2,1);
			} else {
				transform.localScale = new Vector3 (-1, 1, 1);
				for (int i = 0; i < children; ++i)
					transform.GetChild(i).transform.localScale = new Vector3(-2,2,1);
			}
			p1Animator.SetBool ("Moving", animMoving);
			p1Animator.SetBool ("Jumping", animJumping);
		}
	}

	public void pauseAnimation(bool b) {
		p1Animator.enabled=!b;
	}

	void handleStandardMovement ()
	{
		if(rewindScript.movementBarNotFull()) //HAD TO ADD THIS TO FIX A BUG -- NOT SURE WHY IT'S NOT NEEDED IN NETWORKED VERSION
		{
			Vector2 vel = rigidbody2D.velocity;
			if (Mathf.Abs (move) >= joystickMoveThreshold) {
				vel.x = move * maxSpeed;
				animMoving = true;
				if(vel.x > 0) {
					animDirectionRight = true;
				}
				else {
					animDirectionRight = false;
				}
			}
			else {
				vel.x = 0;
				animMoving = false;
			}
			vel.y = rigidbody2D.velocity.y;

			//if (jump && Mathf.Abs(vel.y) <= .6)
			if(jump && canJump) {
				//vel.y += vertSpeed;
				vel.y = vertSpeed;
				animJumping= true;
				animMoving = false;
				//Debug.Log("Jumping");
			}
			else if(vel.y == 0) {
				animJumping = false;
			}
			rigidbody2D.velocity = vel;
		}
	}
	
	void handleLadderMovement()
	{
		if(rewindScript.movementBarNotFull()) //HAD TO ADD THIS TO FIX A BUG -- NOT SURE WHY IT'S NOT NEEDED IN NETWORKED VERSION
		{
			Vector2 vel = Vector2.zero;
			if (Mathf.Abs (vertMove) >= joystickMoveThreshold)
				vel.y = -vertMove * maxSpeed;
			
			if(vel.y > 0 && renderer.bounds.max.y >= ladderTop)
				vel.y = 0;
			
			//break off ladder if jump or left or right is pressed (just like Braid :P )
			if (jump || Mathf.Abs(move) > .4) {
				breakFromLadder();
			}

			if(vel.y != 0) {
				animJumping = false;
				animMoving=true;
			} 
			else {
				animJumping = false;
				animMoving = false;
			}

			rigidbody2D.velocity = vel;
		}
	}
	
	void breakFromLadder()
	{
		onLadder = false;
		rigidbody2D.gravityScale = 1.0f;
	}
	
	public bool getGrabbed() {
		return grabbed;
	}
	
	//returns true if the players are close enough for "grabbing"
	bool close() {
		int maxDistance = 1;
		return (Mathf.Abs(renderer.bounds.center.x - p2.renderer.bounds.center.x) < maxDistance && Mathf.Abs(renderer.bounds.center.y - p2.renderer.bounds.center.y) < maxDistance);
	}


	void setCollision(bool b) {
		if (b) {
			enableCollider(gameObject.collider2D);
		} else {
			disableCollider(gameObject.collider2D);
		}
	}

//	void OnGUI() {
//		
//		GUI.Label (new Rect(0,50,500,500), ((int)Mathf.Abs (rigidbody2D.velocity.y)).ToString());
//
//	}
	

	void setKinematicLocal(bool k)
	{
		rigidbody2D.isKinematic = k;
		if (k)
			p1Animator.enabled = false;
		else
			p1Animator.enabled = true;

	}
	

	void setGrabbed(bool g)
	{
		grabbed = g;
	}


	public void grabPlayerLocal() {
		grabbed = true;
		setKinematicLocal (true);
	}
	

	public void setVelocityLocal(Vector3 v) {
		rigidbody2D.velocity = new Vector2 (v.x, v.y);
	}
	

	public void setPositionLocal(Vector3 v) {
		transform.position = v;
	}
	

	public void dropPlayerLocal(Vector3 pos)
	{
		grabbed = false;
		setPositionLocal (pos);
		setVelocityLocal (new Vector3 (0, 0, 0));
		setKinematicLocal (false);
	}

	public void startShadowfication(float secondsUntilShadowfied){
		shadowficationDuration = secondsUntilShadowfied;
		StartCoroutine("shadowfication");
	}

	IEnumerator shadowfication()
	{
		turningIntoShadows = true;
		float timer = 0.0f;
		while(timer < shadowficationDuration) 
		{
			timer += Time.deltaTime;
			percentShadowed = timer / shadowficationDuration;
			yield return null;
		}
	}
}
