﻿using UnityEngine;
using System.Collections;

public class Player2Control : ControlScript {

	private RewindScript p1rewind;
	private GameObject player1;
	private Player1Control p1script;
	private NetworkView p1view;
	private ToggleableCamera cameraScript;

	private bool p1Grabbed;
	private NetworkView camView;
	private GameManager manager;

 	private Animator p2Animator;
	private bool animMoving;
	private bool animJumping;
	private bool animDirectionRight;

	private bool canJump;
	private bool hasP1info = false;
	private bool tryingToRestart;

	private float p2Color;
	private SpriteRenderer p2SpriteRenderer;
	
	private float percentShadowed;
	private bool turningIntoShadows;
	private float shadowficationDuration;

	void Start () {
		grabbed 	= false;
		gameStarted = true;
		manager 	= (GameManager)FindObjectOfType(typeof(GameManager));
		onLadder = false;
		voidLayer = LayerMask.NameToLayer ("Void");

		//manager needs access to player 2's scripts
		manager.getPlayer2 (networkView.viewID);

		if (Network.isClient) {
			renderer.sortingOrder = 8;
			cameraScript = (ToggleableCamera)FindObjectOfType (typeof(ToggleableCamera));
			camView = cameraScript.networkView;
			camView.RPC("initialize", RPCMode.All, null);
		}

		p2Animator = this.GetComponent<Animator>();
		//initialize animator vars
		animMoving = false;
		animJumping = false;
		animDirectionRight = true;
		p2Animator.SetBool("DirectionRight", animDirectionRight);
		p2Animator.SetBool("Moving", animMoving);
		p2Animator.SetBool("Jumping", animJumping);

		canJump = false;
		tryingToRestart = false;

		p2Color = 0.0f;
		p2SpriteRenderer = this.GetComponent<SpriteRenderer> ();
		
		percentShadowed = 0.0f;
		turningIntoShadows = false;
		shadowficationDuration = 0.0f;
	}

	void Update()
	{

		if (!hasP1info) {
			try {
				p1rewind 	= (RewindScript)FindObjectOfType (typeof(RewindScript));
				player1  	= p1rewind.gameObject; //Object reference not set to instance of object
				p1script 	= player1.GetComponent<Player1Control>();
				p1view   	= player1.GetComponent<NetworkView>();
				player1.networkView.RPC ("getOtherPlayer", RPCMode.All, networkView.viewID);

				if (Network.isClient) {
					renderer.sortingOrder = 1;
					cameraScript = (ToggleableCamera)FindObjectOfType (typeof(ToggleableCamera));
					camView = cameraScript.networkView;
					camView.RPC("initialize", RPCMode.All, null);
				}

				hasP1info = true;

			} catch {
			}
		}

		updateInputs(ref tryingToRestart, ref move, ref vertMove, ref jump, ref breakOff, ref tryingToGrab, ref p1Grabbed);

	}

	void OnCollisionStay2D(Collision2D col)
	{
		if(col.gameObject.tag.ToString() == "Jumpable")
		{
			canJump = true;
		}
	}

	void FixedUpdate () { //FixedUpdate is good for physics stuff -- has nothing to do with time
		if (turningIntoShadows) {
			p2Color = 1.0f - 0.95f * percentShadowed;
			p2SpriteRenderer.color = new Vector4 (p2Color, p2Color, p2Color, 1);
			networkView.RPC("setPercentShadowed", RPCMode.All, percentShadowed);
		}
		if (gameStarted && Network.isClient) {
			if (!grabbed) {			
				if (onLadder)
					handleLadderMovement ();
				else
					handleStandardMovement ();

				//Grab code
				if (tryingToGrab && !p1Grabbed && close () && !p1rewind.getBarFull ())
					p1view.RPC ("grabPlayer", RPCMode.AllBuffered, null);
				if (!tryingToGrab && p1Grabbed)
					p1view.RPC ("dropPlayer", RPCMode.AllBuffered, transform.position);
		
			} else { //grabbed
				transform.position = player1.transform.position;
				//rigidbody2D.velocity = player1.rigidbody2D.velocity;
				rigidbody2D.velocity = Vector2.zero;

				if (breakOff)
					networkView.RPC ("dropPlayer", RPCMode.All, player1.transform.position);
			}
			canJump = false;
			updateAnimation ();
		} else if (gameStarted) {
			updateAnimation ();
		}
	}

	void updateAnimation() {
		int children = transform.childCount;

		if (animDirectionRight) {
			transform.localScale = new Vector3 (1, 1, 1);
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).transform.localScale = new Vector3(2,2,1);
		} else {
			transform.localScale = new Vector3 (-1, 1, 1);
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).transform.localScale = new Vector3(-2,2,1);
		}
		p2Animator.SetBool("Moving", animMoving);
		p2Animator.SetBool("Jumping", animJumping);
	}

	public void pauseAnimation(bool b) {
		p2Animator.enabled=!b;
	}

	void updateInputs (ref bool restart, ref float move, ref float vertMove, ref bool jump, ref bool breakOff, ref bool tryingToGrab, ref bool p1Grabbed)
	{
		if(!Network.isClient) return;

		//these are all p1 now because for networked play, we want the client's FIRST controller to control his player (player2)
		if (Input.GetButton ("P1_Restart")) {
			tryingToRestart = true;
			p1view.RPC("p2Restarting", RPCMode.All, null);
		} else if (tryingToRestart) {
			tryingToRestart = false;
			p1view.RPC ("p2NotRestarting", RPCMode.All, null);
		}
		move = Input.GetAxis ("P1_Horizontal");
		vertMove = Input.GetAxis ("P1_Vertical");
		jump = Input.GetButtonDown ("P1_Jump");
		breakOff = Input.GetButtonDown ("P1_BreakOff");
		tryingToGrab = Input.GetButton("P1_Action");
		if (hasP1info)
			p1Grabbed = p1script.getGrabbed();
	}

	void handleStandardMovement ()
	{
		Vector2 vel = rigidbody2D.velocity;
		if (Mathf.Abs (move) >= joystickMoveThreshold) {
			vel.x = move * maxSpeed;
			if(vel.x > 0) {
				animDirectionRight = true;
				animMoving = true;
			}
			else {
				animDirectionRight = false;
				animMoving = true;
			}
		}
		else {
			vel.x = 0;
			animMoving = false;
		}
		vel.y = rigidbody2D.velocity.y;
		
		//if (jump && Mathf.Abs(vel.y) <= .6)
		if(jump && canJump) {
			//vel.y += vertSpeed;
			vel.y = vertSpeed;
			animJumping= true;
			animMoving = false;
			//Debug.Log("Jumping");
		}
		else if(vel.y != 0) {
			animJumping = false;
		}
		rigidbody2D.velocity = vel;
	}

	void handleLadderMovement()
	{
		Vector2 vel = Vector2.zero;
		if (Mathf.Abs (vertMove) >= joystickMoveThreshold)
			vel.y = -vertMove * maxSpeed;
		
		if(vel.y > 0 && renderer.bounds.max.y >= ladderTop)
			vel.y = 0;
		
		//break off ladder if jump or left or right is pressed (just like Braid :P )
		if (jump || Mathf.Abs(move) > .4) {
			breakFromLadder();
		}
		
		if(vel.y != 0) {
			animMoving=true;
		} 
		else {
			animMoving = false;
		}
		
		rigidbody2D.velocity = vel;
	}
	
	void breakFromLadder()
	{
		onLadder = false;
		rigidbody2D.gravityScale = 1.0f;
	}

	//returns true if the players are close enough for "grabbing"
	bool close() {
		int maxDistance = 1;
		return (Mathf.Abs(renderer.bounds.center.x - player1.renderer.bounds.center.x) < maxDistance && Mathf.Abs(renderer.bounds.center.y - player1.renderer.bounds.center.y) < maxDistance);
	}

	public bool getGrabbed() {
		return grabbed;
	}

	void disableCollider(Collider2D col) {
		col.gameObject.layer = voidLayer;
	}

	void OnGUI() {
		//GUI.Label (new Rect (0, 50, 500, 500), tryingToGrab.ToString ());
	}

	void enableCollider(Collider2D col)
	{
		col.gameObject.layer = 0;
	}

	public void startShadowfication(float secondsUntilShadowfied){
		shadowficationDuration = secondsUntilShadowfied;
		StartCoroutine("shadowfication");
	}
	
	IEnumerator shadowfication()
	{
		turningIntoShadows = true;
		float timer = 0.0f;
		while(timer < shadowficationDuration) 
		{
			timer += Time.deltaTime;
			percentShadowed = timer / shadowficationDuration;
			yield return null;
		}
	}

	/********** Stream Serialization *******************/

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		//fires up when data to be sent.
		Vector3 pos = Vector3.zero;
		Vector3 vel = Vector3.zero;
		bool moving = false;
		bool jumping = false;
		bool dirRight = true;
		//bool g = false; //grabbed - dunno if it should be set to false...
		if (stream.isWriting) { //we want to send data
			pos = transform.position;
			vel = rigidbody2D.velocity;
			moving = animMoving;
			jumping = animJumping;
			dirRight = animDirectionRight;
			stream.Serialize(ref pos);
			stream.Serialize(ref vel);
			stream.Serialize(ref moving);
			stream.Serialize(ref jumping);
			stream.Serialize(ref dirRight);
		} else { //we deal with received data
			stream.Serialize(ref pos);
			transform.position = pos;
			stream.Serialize(ref vel);
			rigidbody2D.velocity = vel;
			stream.Serialize(ref moving);
			animMoving = moving;
			stream.Serialize(ref jumping);
			animJumping = jumping;
			stream.Serialize(ref dirRight);
			animDirectionRight = dirRight;
		}
	}



	/********** Remote Procedure Calls *******************/

	[RPC]
	void setKinematic(bool b) {
		gameObject.rigidbody2D.isKinematic = b;
	}

	[RPC]
	void setCollision(bool b) {
		if (b) {
			enableCollider(gameObject.collider2D);
		} else {
			disableCollider(gameObject.collider2D);
		}
	}


	[RPC] 
	void setVelocity(Vector3 v) {
		rigidbody2D.velocity = v;
	}

	[RPC]
	void setPosition(Vector3 v) 
	{
		transform.position = v;
	}

	[RPC]
	void dropPlayer(Vector3 pos)
	{
		grabbed = false;
		networkView.RPC("setPosition", RPCMode.All, pos);
		networkView.RPC("setVelocity", RPCMode.All, new Vector3(0,0,0));
		networkView.RPC("setKinematic", RPCMode.All, false);
	}

	[RPC]
	void grabPlayer() {
		grabbed = true;
		networkView.RPC("setKinematic", RPCMode.All, true);
	}

	[RPC]
	void disable()
	{
		enabled = false;
	}
	[RPC]
	void enable()
	{
		enabled = true;
	}

	[RPC]
	void setPercentShadowed(float perSh)
	{
		percentShadowed = perSh;
	}
}