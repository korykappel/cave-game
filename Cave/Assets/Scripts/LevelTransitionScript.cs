﻿using UnityEngine;
using System.Collections;

public class LevelTransitionScript : MonoBehaviour {

	GameManager manager;
	public enum Level {LEVEL1=0, LEVEL2=1};
	public Level currentLevel;

	// Use this for initialization
	void Start () {
		manager = (GameManager)FindObjectOfType (typeof(GameManager));
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D(Collider2D col)
	{
		if(col.gameObject.tag == "P1" || col.gameObject.tag == "P2")
		{
			manager.loadNextLevel();
		}
	}
}
