﻿using UnityEngine;
using System.Collections;

public class ControlScript : MonoBehaviour {

	public float maxSpeed = 10f;
	public float vertSpeed = 30f;
	public float joystickMoveThreshold = 1f;
	
	protected GameObject cam;
	protected float move;
	protected bool jump;
	protected bool grabbed;
	protected bool tryingToGrab;
	protected bool gameStarted;
	protected bool breakOff;
	protected int oldLayer = -1;
	protected int voidLayer;
	protected bool paused = false;
	
	//make these private and make set functions later
	protected bool onLadder;
	protected float ladderTop;
	protected float ladderBottom;
	protected float vertMove;

	public void setOnLadder(bool b)
	{
		onLadder = b;
	}
	public void setLadderTop(float b)
	{
		ladderTop = b;
	}
	public void setLadderBottom(float b)
	{
		ladderBottom = b;
	}
	public void setVertMove(float b)
	{
		vertMove = b;
	}

	public bool getOnLadder()
	{
		return onLadder;
	}
	public float getVertMove()
	{
		return vertMove;
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
