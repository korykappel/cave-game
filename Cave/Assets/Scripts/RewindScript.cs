﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RewindScript : FreezableObject {
	
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public int extraFrames;
	public int maxFrames;
	
	//Bar stuff
	private float percentFull; //current progress
	private float p1Color;
	private Vector2 pos;   
	private Vector2 size;
	private bool barFull;
	
	private int maxElapsedFrames;
	private int numFramesElapsed;
	private float xPosLastFrame, yPosLastFrame;
	private Stack horizontalTransforms = new Stack();
	private Stack verticalTransforms = new Stack();
	private List<Vector2> velocityStack = new List<Vector2>();
	private float initialPositionX, initialPositionY;
	private bool rewind;
	private Vector2 mBoxSize;
	private bool wasJustRewinding = false;
	private bool powerEnabled = false;

	private ClockHandScript clockHandScript;
	private Player1Control player1script;
	private Animator p1Animator;
	private SpriteRenderer p1SpriteRenderer;
	private FreezeScript freezeScript;
	
	
	// Use this for initialization
	void Start () {
		size = new Vector2(300,20);
		pos = new Vector2 (20, 40);
		initialPositionX = transform.position.x;
		initialPositionY = transform.position.y;
		xPosLastFrame = transform.position.x - 20.0f;
		yPosLastFrame = transform.position.y - 20.0f;
		numFramesElapsed = 0;
		mBoxSize = (collider2D as BoxCollider2D).size;
		barFull = false;
		maxElapsedFrames = maxFrames * (extraFrames + 1);
		p1Color = 0.0f;
		
		clockHandScript = (ClockHandScript)FindObjectOfType (typeof(ClockHandScript));
		player1script = (Player1Control)FindObjectOfType (typeof(Player1Control));
		p1Animator = gameObject.GetComponent<Animator>();
		p1SpriteRenderer = this.GetComponent<SpriteRenderer> ();
		freezeScript = (FreezeScript)FindObjectOfType<FreezeScript> ();
	}
	
	void FixedUpdate () { 
		
		percentFull = numFramesElapsed/(float)maxElapsedFrames;
		//clockHandScript.setHand (percentFull);
		//p1Color = 1.0f - 0.92f * percentFull;
		//p1SpriteRenderer.color = new Vector4(p1Color, p1Color, p1Color, 1);

		rewind = Input.GetButton("Rewind");

		if (powerEnabled && !freezeScript.frozen) {
			if (rewind && !player1script.getGrabbed()) {
				if(!movementBarNotFull()) //call barNotFilled when bar was filled and rewind pressed
					networkView.RPC ("barNotFilled", RPCMode.All, null);
				Rewind();
			} else {
				if (wasJustRewinding) {
					rigidbody2D.velocity = -rigidbody2D.velocity;
					wasJustRewinding = false;
					networkView.RPC ("setKinematic", RPCMode.All, false);
				}
				if(!movementBarNotFull()) 
					networkView.RPC ("barFilled", RPCMode.All, null);
				else {
					recordPositionIfMoved ();
				}
			}
		}
	}
	
	void Rewind()
	{
		networkView.RPC ("setKinematic", RPCMode.All, true);
		wasJustRewinding = true;
		if (horizontalTransforms.Count > 0) {
			//transform.position = Vector2.Lerp(transform.position, new Vector2 ((float)horizontalTransforms.Pop (), (float)verticalTransforms.Pop ()), Time.deltaTime * speedFactor);
			transform.position = new Vector2 ((float)horizontalTransforms.Pop (), (float)verticalTransforms.Pop ());
			//rigidbody2D.velocity = velocityStack[velocityStack.Count - 1] * -1;
			velocityStack.RemoveAt(velocityStack.Count - 1);
			numFramesElapsed--;
		} else {
			rigidbody2D.velocity = new Vector2 (0, 0);
			transform.position = new Vector2(initialPositionX, initialPositionY);
		}
	}
	
	void recordPositionIfMoved ()
	{
		//if i decrease 0.02, increase max frames
		if (diffGTX (transform.position.x, xPosLastFrame, 0.02) || diffGTX (transform.position.y, yPosLastFrame, 0.02)) {
			
			pushExtraFrames();
			
			horizontalTransforms.Push (gameObject.transform.position.x);
			verticalTransforms.Push (gameObject.transform.position.y);
			velocityStack.Add(gameObject.rigidbody2D.velocity);
			numFramesElapsed += (1 + extraFrames);
			//numFramesElapsed += 1;
			xPosLastFrame = transform.position.x;
			yPosLastFrame = transform.position.y;
		}
	}
	
	void pushExtraFrames ()
	{
		float p1 = xPosLastFrame;
		float p2 = yPosLastFrame;
		float q1 = transform.position.x;
		float q2 = transform.position.y;
		//Vector2 P = new Vector2(p1, p2);
		//Vector2 Q = new Vector2(q1, q2);
		
		int divisor = extraFrames + 1;
		for (int i = 1; i < extraFrames + 1; i++) {
			float f = i/divisor;
			Vector2 newPoint = new Vector2(f*p1 + (1-f)*q1, f*p2 + (1-f)*q2);
			
			horizontalTransforms.Push(newPoint.x);
			verticalTransforms.Push(newPoint.y);
			velocityStack.Add(gameObject.rigidbody2D.velocity);
		}
	}
	
	bool diffGTX (float num1, float num2, double x)
	{
		return (Mathf.Abs(num1-num2) > x);
	}
	
	public bool movementBarNotFull() {
		return (numFramesElapsed < maxElapsedFrames);
	}
	
//	void OnGUI() {
//		//draw the background:
//		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
//		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex);
//		
//		//draw the filled-in part:
//		GUI.BeginGroup(new Rect(0,0, size.x * percentFull, size.y));
//		GUI.Box(new Rect(0,0, size.x, size.y), fullTex);
//		GUI.EndGroup();
//		GUI.EndGroup();
//	}

//		void OnGUI() {
//				GUI.Label (new Rect (0, 50, 500, 500), player1script.getGrabbed ().ToString ());
//		}
	
	public bool getBarFull() {
		return barFull;
	}

	public float getPercentFull() {
		return percentFull;
	}

//	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
//		//fires up when data to be sent.
//		float perFull = 0.0f;
//		//bool g = false; //grabbed - dunno if it should be set to false...
//		if (stream.isWriting) { //we want to send data
//			perFull = percentFull;
//			stream.Serialize(ref perFull);
//		} else { //we deal with received data
//			stream.Serialize(ref perFull);
//			percentFull = perFull;
//		}
//	}

	[RPC]
	void setKinematic(bool k)
	{
		rigidbody2D.isKinematic = k;
		if (k)
			p1Animator.enabled = false;
		else
			p1Animator.enabled = true;
	}
	
	[RPC]
	void barFilled()
	{
		rigidbody2D.velocity = new Vector2 (0, 0);
		barFull = true;
		rigidbody2D.isKinematic = true;
		p1Animator.enabled = false;
	}

	[RPC]
	void barNotFilled()
	{
		barFull = false;
		rigidbody2D.isKinematic = false;
		p1Animator.enabled = true;
	}

	[RPC]
	void resetRewind()
	{
		numFramesElapsed = 0;
		barFull = false;
		rigidbody2D.isKinematic = false;
		p1Animator.enabled = true;
		horizontalTransforms.Clear ();
		verticalTransforms.Clear ();
		velocityStack.Clear ();
		initialPositionX = transform.position.x;
		initialPositionY = transform.position.y;
	}

	[RPC]
	void setPowerEnabled(bool b)
	{
		powerEnabled = b;
	}

	public bool getPowerEnabled()
	{
		return powerEnabled;
	}
	
}
