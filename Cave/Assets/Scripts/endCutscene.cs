﻿using UnityEngine;
using System.Collections;

public class endCutscene : MonoBehaviour {

	bool movePlayers;
	float timer, timeToTake;

	Player1ControlLocal p1;
	Player2ControlLocal p2;
	RewindScriptLocal rewind;
	FreezeScriptLocal freeze;

	// Use this for initialization
	void Start () {
		//timer = timeToTake = 0;
	}
	
	// Update is called once per frame
	void Update () {

		timer += Time.deltaTime;
		//Debug.Log(timer);
		if(timer > timeToTake)
		{
			//movePlayers = false;
			//p1.rigidbody2D.velocity = Vector2.zero;
			enablePlayers();
			this.enabled = false;
		}
	}

	void OnEnable()
	{
		disablePlayers ();

		timer = 0;
		timeToTake = 2f;
		//p1.rigidbody2D.velocity = new Vector2(p1.rigidbody2D.velocity.x + 5f, p1.rigidbody2D.velocity.y);
		//movePlayers = true;
		//START FUNCTION IS OVERWRITING THESE VARIABLES

	}

	void disablePlayers()
	{
		//disable player scripts 
		p1 = (Player1ControlLocal)FindObjectOfType (typeof(Player1ControlLocal));
		p2 = (Player2ControlLocal)FindObjectOfType (typeof(Player2ControlLocal));
		rewind = (RewindScriptLocal)FindObjectOfType (typeof(RewindScriptLocal));
		freeze = (FreezeScriptLocal)FindObjectOfType(typeof(FreezeScriptLocal));
		p1.enabled = false;
		p2.enabled = false;
		rewind.enabled = false;
		freeze.enabled = false;
	}

	void enablePlayers()
	{
		//disable player scripts 
		p1.enabled = true;
		p2.enabled = true;
		rewind.enabled = true;
		freeze.enabled = true;
	}
}
