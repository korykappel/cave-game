﻿using UnityEngine;
using System.Collections;

public class FreezeScriptLocal : MonoBehaviour {
	
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public int maxElapsedFrames = 200;
	
	//freeze stuff
	private movingPlatformScript platform1;
	private FreezableObject[] freezableObjects;
	private int freezeLeft;
	private bool frozen;
	private bool skipThisFrame = false;
	private bool powerEnabled = false;
	
	//Bar stuff
	private float barDisplay; //current progress
	private Vector2 pos;   
	private Vector2 size;
	
	//Reference to screen tint
	private ScreenTint tintScript;
	
	//Sound stuff
	private AudioSource freezeSound;
	private GameObject musicObject;
	private AudioSource mainMusic;

	private ParticleEmitter[] emitter;

	private Player2ControlLocal player2script;
	
	
	// Use this for initialization
	void Start () {
		freezableObjects = (FreezableObject[])FindObjectsOfType (typeof(FreezableObject));
		tintScript = (ScreenTint)FindObjectOfType (typeof(ScreenTint));
		
		size = new Vector2(300,20);
		pos = new Vector2 (600, 40);
		
		frozen = false;
		freezeLeft = maxElapsedFrames;
		
		//audio stuff
		freezeSound = (AudioSource)gameObject.AddComponent ("AudioSource");
		AudioClip myAudioClip; 
		myAudioClip = (AudioClip)Resources.Load ("SFX/whoosh");
		freezeSound.clip = myAudioClip;
		musicObject = GameObject.FindGameObjectWithTag("MainMusic");
		mainMusic = musicObject.GetComponent<AudioSource> ();

		emitter = (ParticleEmitter[])FindObjectsOfType (typeof(ParticleEmitter));
		player2script = (Player2ControlLocal)FindObjectOfType (typeof(Player2ControlLocal));
	}
	
	void Update () {

		barDisplay = freezeLeft / (float)maxElapsedFrames;

		if(powerEnabled)
		{
			if(Input.GetButtonDown("Freeze") && !player2script.getGrabbed())
			{
				for(int i=0; i<emitter.Length; i++) {
					emitter[i].enabled = false;
				}

				tintScript.setTintLocal(true);
				frozen = true;
				playFreezeSoundLocal ();
				for(int i=0; i<freezableObjects.Length; i++)
					freezableObjects[i].freezeLocal();
			}
			if(frozen && Input.GetButtonUp ("Freeze"))
			{
				turnFreezeOffLocal();
			}
			
			if (frozen) {
				tintScript.setFreezeLeft(barDisplay);
				if (skipThisFrame) {
					freezeLeft--;
					skipThisFrame = !skipThisFrame;
				}
				else skipThisFrame = !skipThisFrame;
			}
			else if(freezeLeft <= maxElapsedFrames) freezeLeft++;
			
			if(freezeLeft <= 0) {
				turnFreezeOffLocal();
			}
		}
	}
	
	public bool getFrozen() {
		return frozen;
	}
	

	void playFreezeSoundLocal()
	{
		freezeSound.Play();
		mainMusic.Pause ();
	}
	

	void stopFreezeSoundLocal()
	{
		freezeSound.Stop();
		mainMusic.Play();
	}

	public void enablePowerLocal()
	{
		powerEnabled = true;
	}
	public void disablePowerLocal()
	{
		powerEnabled = false;
		turnFreezeOffLocal ();
	}
	public void resetFreezeLocal()
	{
		freezeLeft = maxElapsedFrames;
	}

	void turnFreezeOffLocal()
	{
		if(!enabled) return;

		for(int i=0; i<emitter.Length; i++) {
			emitter[i].enabled = true;
		}

		tintScript.setTintLocal (false);
		frozen = false;
		stopFreezeSoundLocal ();
		for(int i=0; i<freezableObjects.Length; i++)
			freezableObjects[i].unfreezeLocal ();
	}
	
//	void OnGUI() {
//		//		for(int i=0; i<freezableObjects.Length; i++)
//		//			GUI.Label (new Rect(0,50*(i+1),500,500),freezableObjects[i].ToString());
//
//		//draw the background:
//		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
//		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex);
//		
//		//draw the filled-in part:
//		GUI.BeginGroup(new Rect(0,0, size.x * barDisplay, size.y));
//		GUI.Box(new Rect(0,0, size.x, size.y), fullTex);
//		GUI.EndGroup();
//		GUI.EndGroup();
//	}
}
