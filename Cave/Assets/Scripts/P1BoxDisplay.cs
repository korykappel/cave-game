﻿using UnityEngine;
using System.Collections;

public class P1BoxDisplay: MonoBehaviour {
	
	SpriteRenderer renderer;

	Vector3 convoPosition = new Vector3(2.1f, 2.5f, 0);
	Vector3 restartPosition = new Vector3(0,1.5f,0);
	Vector3 pausedPosition = new Vector3(0,2.2f,0);

	Transform p1;

	// Use this for initialization
	void Start () {
		
		renderer = GetComponent<SpriteRenderer> ();
		renderer.enabled = true;
	}

	[RPC]
	void setImage(string image)
	{
		renderer.sprite = Resources.Load<Sprite>("Images/" + image);
		if(image.ToString().Contains("TryingToRestart"))
			transform.position = p1.position + restartPosition;
		else if(image.ToString().Contains("convo"))
			transform.position = p1.position + convoPosition;
		else if(image.ToString().Contains("Paused_small"))
			transform.position = p1.position + pausedPosition;
	}

	[RPC]
	void initialize()
	{
		p1 = GameObject.Find ("Player1(Clone)").transform;
		transform.position = p1.position + convoPosition;
		transform.parent = p1;
	}
	
	//IEnumerator designates this function as a coroutine, which can return after one frame and pick up execution in the next
	public IEnumerator displayForXSeconds(float seconds)
	{
		if(Network.isServer)
		{
			networkView.RPC ("setVisible", RPCMode.All, true);
			float timer = 0;
			while(timer < seconds)
			{
				timer += Time.deltaTime;
				yield return null;
			}
			networkView.RPC ("setVisible", RPCMode.All, false);
		}
	}

	[RPC]
	void setVisible(bool b)
	{
		renderer.enabled = b;
	}
}
