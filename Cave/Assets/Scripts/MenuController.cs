﻿using UnityEngine;
using System.Collections;

/*
How does this work?

All game Objects that are menu items are tagged with "MenuItem"
In this script, all items tagged with "MenuItem" are loaded into the array menuItems
The first object tagged "MenuItem" will be at index 0 in menuItems

Then there is an array of displayed menuitems. This ArrayList contains all the 
	menu items that are currently being displayed on the screen.

The variable "hovered" contains the index of themenu item currently being hovered over, relative
	to the displayed items.

Example:
If there are menu items: 1, 2, 3, 4, 5, and 6
But only 3, 4, and 5 are showing
And Menu item 4 is currently hovered over

Then the variables would look as follows:
menuItems: {1, 2, 3, 4, 5, 6}
displayed: {3, 4, 5}
hovered: 1				//The first index in displayed
 */
using System.Collections.Generic;


public class MenuController : MonoBehaviour {

	private GameObject[] menuItems;
	private GameObject[] backdrops;
	private GameObject gameName;
	private GameManager manager;

	private GameObject[] controlsMenu;
	private GameObject waitingOnP2Screen;	

	private List<GameObject> objectsToHide = new List<GameObject>();

	private ArrayList currentConfiguration = new ArrayList();
	private ArrayList gameModeButtons = new ArrayList();
	private ArrayList newOrContinueButtons = new ArrayList();
	private ArrayList optionsButtons = new ArrayList();

	private Color hoveredColor;
	private Color normalColor;

	private int hovered;
	private bool focusChanged = true;

	//control variables
	private float selectDirection;
	private bool selectPressed;
	private bool downDebounced;
	private bool upDebounced;

	enum GameMode {UNINIT, LOCAL, ONLINE};
	enum GameStart {UNINIT, CONTINUE, NEW};
	enum MenuState {MAIN, SUB, OPTIONS, DONE, PAUSE, DISCONNECT};
	private GameMode gameMode;
	private GameStart gameStart;
	private MenuState menuState;
	private MenuState menuStateLastFrame;
	private bool updating = true;
	private bool waitingScreenNotSet;

	//Sound stuff
	private AudioSource menuSelectSound;
	private AudioSource menuChooseSound;
	private AudioSource mainMusic;
	private GameObject musicObject;

	// Use this for initialization
	void Start() {

		gameMode = GameMode.UNINIT;
		gameStart = GameStart.UNINIT;
		menuState = MenuState.MAIN;
		menuStateLastFrame = MenuState.OPTIONS; //just needs to be diff from menuState to start

		upDebounced = true;
		downDebounced = true;
		hoveredColor = new Color (1.0f, 1.0f, 1.0f, 1.0f);
		normalColor = new Color (0.50f, 0.50f, 0.50f, 1.0f);

		//initialization
		menuItems = GameObject.FindGameObjectsWithTag("MenuItem");
		backdrops = GameObject.FindGameObjectsWithTag("Backdrop");
		controlsMenu = GameObject.FindGameObjectsWithTag("Controls");
		waitingOnP2Screen = GameObject.FindGameObjectWithTag("waitingOnP2");
		gameName = GameObject.FindGameObjectWithTag("GameName");
		manager = (GameManager)FindObjectOfType(typeof(GameManager));
		objectsToHide.Add (GameObject.FindGameObjectWithTag ("inGamePause"));
		objectsToHide.Add (GameObject.FindGameObjectWithTag ("sorryScreen"));
		objectsToHide.Add (GameObject.FindGameObjectWithTag ("gameNameEnd"));

		hideObjectsThatShouldBeHidden();
		//menu items are found in an arbitrary order. this sorts them so they are
		//in numerical order based on the number in the name of the gameobject
		sortItems();  

		//defaults
		selectDirection = 0.0f;
		selectPressed = false;

		gameModeButtons.Add(0);
		gameModeButtons.Add(1);
		gameModeButtons.Add(2);
		newOrContinueButtons.Add(3);
		newOrContinueButtons.Add(4);
		newOrContinueButtons.Add(5);
		optionsButtons.Add(6);

		setConfiguration(gameModeButtons);
		waitingScreenNotSet = true;

		//audio stuff
		menuSelectSound = (AudioSource)gameObject.AddComponent ("AudioSource");
		AudioClip myAudioClip; 
		myAudioClip = (AudioClip)Resources.Load ("SFX/menu option");
		menuSelectSound.clip = myAudioClip;
		menuChooseSound = (AudioSource)gameObject.AddComponent ("AudioSource");
		myAudioClip = (AudioClip)Resources.Load ("SFX/menu choose");
		menuChooseSound.clip = myAudioClip;
		musicObject = GameObject.FindGameObjectWithTag("MainMusic");
		mainMusic = musicObject.GetComponent<AudioSource> ();
	}

	void Update()
	{


		if (!updating) return;

		if (waitingScreenNotSet) {
			try {
				waitingOnP2Screen.SetActive(false);
				waitingScreenNotSet = false ;
			} catch  {	}
		}

		if (menuState != menuStateLastFrame) {
			if (menuState != MenuState.OPTIONS)
				controlsMenu[0].SetActive(false);
			focusChanged = true;
			hovered = 0;
			if (menuState == MenuState.MAIN) {
				//Debug.Log ("Setting menu state to MAIN");
				setConfiguration(gameModeButtons);
			} else if (menuState == MenuState.SUB) {
				setConfiguration(newOrContinueButtons);
				//Debug.Log ("Setting menu state to SUB");
			} else if (menuState == MenuState.OPTIONS) {
				setConfiguration(optionsButtons);
				controlsMenu[0].SetActive(true);
				//Debug.Log ("Setting menu state to OPTIONS");
			} else if (menuState == MenuState.DONE) {
				startGame();
//				Debug.Log ("Setting menu state to DONE");
			} else if (menuState == MenuState.PAUSE) {

//				Debug.Log ("Menu State to PAUSE");
			} else if (menuState == MenuState.DISCONNECT) {

//				Debug.Log ("Menu State to DISCONNECT");
			}
			menuStateLastFrame = menuState;
		}

		selectDirection = Input.GetAxis("MenuControl");
		selectPressed = Input.GetButtonDown("Select"); 

		if (selectDirection > -0.3f) 
			upDebounced = true;
		if (selectDirection < 0.3f)
			downDebounced = true;
	}

	void FixedUpdate() {


		if (!updating) return;
		if (selectDirection <= -0.9 && upDebounced) { //D pad up
			upDebounced = false;
			if (hovered != 0) {
				menuSelectSound.Play ();
				hovered--;
				focusChanged = true;
			}
		}

		if (selectDirection >= 0.9 && downDebounced) { //D pad down
			downDebounced = false;
			if (hovered != currentConfiguration.Count - 1) {
				menuSelectSound.Play ();
				hovered++;
				focusChanged = true;
			}
		}

		if (focusChanged) {
			for (int i = 0; i < currentConfiguration.Count; i++)
				setIndexColor(i, normalColor);
		    setIndexColor(hovered, hoveredColor);
			focusChanged = false;
		}

		if (selectPressed) {

			menuChooseSound.Play();

			if (menuState == MenuState.MAIN) {
				if(hovered == 0) {
					gameMode = GameMode.LOCAL;
					menuState = MenuState.SUB;
				} else if(hovered == 1) {
					gameMode = GameMode.ONLINE;
					menuState = MenuState.SUB;
				} else if(hovered == 2) {
					menuState = MenuState.OPTIONS;
				} else {
					throw new UnityException("Option selected that shouldn't exist. Hovered == " + hovered.ToString());
				}
			} else if (menuState == MenuState.SUB) {
				if(hovered == 0) {
					gameStart = GameStart.CONTINUE;
					menuState = MenuState.DONE;
				} else if(hovered == 1) {
					gameStart = GameStart.NEW;
					menuState = MenuState.DONE;
				} else if(hovered == 2) {
					menuState = MenuState.MAIN;
				} else {
					throw new UnityException("Option selected that shouldn't exist.");
				}
			} else if (menuState == MenuState.OPTIONS) {
				if (hovered == 0) {
					menuState = MenuState.MAIN;
				} else {
					throw new UnityException("Option selected that shouldn't exist.");
				}
			} else {
				throw new UnityException("In a submenu that doesn't exist.");
			}

			selectPressed = false;
		}
	}

	void sortItems ()
	{
		GameObject[] newItems = (GameObject[])menuItems.Clone ();
		List<int> itemNums = new List<int>();
		for (int i = 0; i < newItems.Length; i++) {
			itemNums.Add(int.Parse(newItems[i].name.Substring(newItems[i].name.IndexOf('_')+1)));
		}

		//Print order menu items were found
		//string temp = "";
		//for (int i = 0; i < itemNums.Count; i++)
		//	temp += itemNums[i].ToString() + " ";
		//Debug.Log(temp);

		//Sort numList
		itemNums.Sort();

		//Order object list according to sortednumList
		for (int i = 0; i < itemNums.Count; i++) {
			int menuNum = 0;
			for (int j = 0; j < menuItems.Length; j++) {
				if (menuItems[j].name.Contains(itemNums[i].ToString())) {
					menuNum = j;
					break;
				}
			}
			newItems[i] = menuItems[menuNum];
		}
		
		menuItems = newItems;
		
	}

	void startGame ()
	{
		disableEverything ();

		mainMusic.Play ();
	

		if (gameMode == GameMode.LOCAL)
		{

			if (gameStart == GameStart.NEW)
				manager.setLevel (0);
			else if (gameStart == GameStart.CONTINUE) 
			{
				manager.setLevel(PlayerPrefs.GetInt("localLevel"));
			}
			manager.enableLocalPlay();
		}
		else if (gameMode == GameMode.ONLINE) 
		{
			waitingOnP2Screen.SetActive(true);
			manager.enableNetworkedPlay();
			if (gameStart == GameStart.NEW)
				manager.setLevel(0);
			else if (gameStart == GameStart.CONTINUE) 
				manager.setLevel(PlayerPrefs.GetInt("networkedLevel"));
		}
	}

	void disableEverything()
	{
		for (int i = 0; i < menuItems.Length; i++)
			menuItems [i].SetActive (false);
		for (int i = 0; i < backdrops.Length; i++)
			backdrops [i].SetActive (false);
		gameName.SetActive (false);
		//this.enabled = false;
		updating = false;
	}

	public void enableEverything()
	{
		for (int i = 0; i < menuItems.Length; i++)
			menuItems [i].SetActive (true);
		for (int i = 0; i < backdrops.Length; i++)
			backdrops [i].SetActive (true);
		gameName.SetActive (true);
		updating = true;

		upDebounced = true;
		downDebounced = true;
		gameMode = GameMode.UNINIT;
		gameStart = GameStart.UNINIT;
		menuState = MenuState.MAIN;
		menuStateLastFrame = MenuState.OPTIONS;
		selectDirection = 0.0f;
		selectPressed = false;
		setConfiguration(gameModeButtons);
		waitingScreenNotSet = true;

		mainMusic.Stop ();
	}

//	public void enableAllMenuItems() {
//		for (int i = 0; i < menuItems.Length; i++)
//			menuItems [i].SetActive (true);
//		for (int i = 0; i < backdrops.Length; i++)
//			backdrops [i].SetActive (true);
//		gameName.SetActive (true);
//		updating = true;
//	}

	void setIndexColor(int index, Color c)
	{
		//Debug.Log ("Index is " + index.ToString () + " but menuItems only contains " + menuItems.Length.ToString () + " items and currentConfig only contains " + currentConfiguration.Count + " elements.");
		menuItems[(int)currentConfiguration[index]].renderer.material.color = c;
	}

	bool validSelectedIndex ()
	{
		return !(hovered < 0 || hovered > currentConfiguration.Count);
	}
	
	void reverseItems()
	{
		GameObject[] newItems = (GameObject[])menuItems.Clone ();
		for (int i = 0, j = menuItems.Length - 1; i < menuItems.Length; i++, j--)
			menuItems [i] = newItems [j];
	}

	int getSelected() {
		return hovered;
	}

	void setConfiguration(ArrayList displayed) {
		for (int i = 0; i < menuItems.Length; i++) 
			menuItems[i].gameObject.renderer.enabled = false;

		for (int i = 0; i < displayed.Count; i++)
			menuItems[(int)displayed[i]].gameObject.renderer.enabled = true;

		currentConfiguration = displayed;
	}

	private void hideObjectsThatShouldBeHidden ()
	{
		Debug.Log ("hidden objects: " + objectsToHide.Count);
		for (int i = 0; i < objectsToHide.Count; i++) {
			objectsToHide[i].renderer.material.color = new Color (1,1,1,0);
		}
	}

}
