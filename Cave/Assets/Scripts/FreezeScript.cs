﻿using UnityEngine;
using System.Collections;

public class FreezeScript : MonoBehaviour {
	
	public Texture2D emptyTex;
	public Texture2D fullTex;
	public int maxElapsedFrames = 200;

	//freeze stuff
	private movingPlatformScript platform1;
	private FreezableObject[] freezableObjects;
	private int freezeLeft;
	public  bool frozen;
	private bool skipThisFrame = false;
	private bool powerEnabled = false;

	//Bar stuff
	private float barDisplay; //current progress
	private Vector2 pos;   
	private Vector2 size;

	//Reference to screen tint
	private ScreenTint tintScript;

	//Sound stuff
	private AudioSource freezeSound;
	private GameObject musicObject;
	private AudioSource mainMusic;

	private ParticleEmitter[] emitter;

	private Player2Control player2script;
	
	// Use this for initialization
	void Start () {
		freezableObjects = (FreezableObject[])FindObjectsOfType (typeof(FreezableObject));
		tintScript = (ScreenTint)FindObjectOfType (typeof(ScreenTint));

		size = new Vector2(300,20);
		pos = new Vector2 (600, 40);
		
		frozen = false;
		freezeLeft = maxElapsedFrames;

		//audio stuff
		freezeSound = (AudioSource)gameObject.AddComponent ("AudioSource");
		AudioClip myAudioClip; 
		myAudioClip = (AudioClip)Resources.Load ("SFX/whoosh");
		freezeSound.clip = myAudioClip;
		musicObject = GameObject.FindGameObjectWithTag("MainMusic");
		mainMusic = musicObject.GetComponent<AudioSource> ();

		emitter = (ParticleEmitter[])FindObjectsOfType (typeof(ParticleEmitter));
		player2script = (Player2Control)FindObjectOfType (typeof(Player2Control));
	}
	
	void Update () {
		if (Network.isClient && powerEnabled) {
			barDisplay = freezeLeft / (float)maxElapsedFrames;

			//Debug.Log(Input.GetButtonDown("Rewind"));
			//Debug.Log(!player2script.getGrabbed());
			if(Input.GetButtonDown("Rewind") && !player2script.getGrabbed())
			{

				networkView.RPC("disableParticles", RPCMode.All, null);

				tintScript.networkView.RPC ("setTint", RPCMode.All, true);
				frozen = true;
				networkView.RPC ("playFreezeSound", RPCMode.All, null);
				for(int i=0; i<freezableObjects.Length; i++)
				{
					//freezableObjects[i].frozen = true;
					//Debug.Log(freezableObjects[i]);
					freezableObjects[i].networkView.RPC ("freeze", RPCMode.All, null);
				}
			}
			if(frozen && Input.GetButtonUp ("Rewind"))
			{
				networkView.RPC("turnFreezeOff", RPCMode.All, null);
			}
			
			if (frozen) {
				tintScript.networkView.RPC ("setFreezeLeft", RPCMode.All, barDisplay);
				if (skipThisFrame) {
					freezeLeft--;
					skipThisFrame = !skipThisFrame;
				}
				else skipThisFrame = !skipThisFrame;
			}
			else if(freezeLeft <= maxElapsedFrames) freezeLeft++;
			
			if(freezeLeft <= 0) {
				networkView.RPC("turnFreezeOff", RPCMode.All, null);
			}
		}
	}

	[RPC]
	void disableParticles()
	{
		for(int i=0; i<emitter.Length; i++) {
			emitter[i].enabled = false;
		}
	}

	[RPC]
	void turnFreezeOff()
	{
		for(int i=0; i<emitter.Length; i++) {
			emitter[i].enabled = true;
		}

		tintScript.networkView.RPC ("setTint", RPCMode.All, false);
		frozen = false;
		networkView.RPC ("stopFreezeSound", RPCMode.All, null);
		for(int i=0; i<freezableObjects.Length; i++)
			freezableObjects[i].networkView.RPC ("unfreeze", RPCMode.All, null);
	}

	[RPC]
	void enablePower()
	{
		powerEnabled = true;
	}

	[RPC]
	void disablePower()
	{
		powerEnabled = false;
		turnFreezeOff ();
	}


	public bool getFrozen() {
		return frozen;
	}

	[RPC]
	void playFreezeSound()
	{
		freezeSound.Play();
		mainMusic.Pause ();
	}

	[RPC]
	void stopFreezeSound()
	{
		freezeSound.Stop();
		if(!mainMusic.isPlaying)
			mainMusic.Play();
	}	

	[RPC]
	void resetFreeze()
	{
		freezeLeft = maxElapsedFrames;
	}

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		//fires up when data to be sent.
		bool freeze = false; //grabbed - dunno if it should be set to false...
		if (stream.isWriting) {
			freeze = frozen;
			stream.Serialize(ref freeze);
			
		} else { //This could be an else since funct only fires when data xfer happens
			stream.Serialize(ref freeze);
			frozen = freeze;
		}
	}
}
