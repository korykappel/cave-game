﻿using UnityEngine;
using System.Collections;

public class PausedLocal : MonoBehaviour {

	RewindScriptLocal rewindscript;
	FreezeScriptLocal freezescript;
	Player1ControlLocal p1script;
	Player2ControlLocal p2script;
	//P1BoxDisplayLocal boxdisplay;
	MenuController menu;
	private GameObject inGamePause;

	ToggleableCameraLocal cam; //needed for setting location of menu screen?

	private bool paused;

	// Use this for initialization
	void Start () {
		rewindscript = (RewindScriptLocal)FindObjectOfType<RewindScriptLocal> ();
		freezescript = (FreezeScriptLocal)FindObjectOfType<FreezeScriptLocal> ();
		p1script = (Player1ControlLocal)FindObjectOfType<Player1ControlLocal> ();
		p2script = (Player2ControlLocal)FindObjectOfType<Player2ControlLocal> ();
		menu = (MenuController)FindObjectOfType<MenuController> ();
		cam = (ToggleableCameraLocal)FindObjectOfType<ToggleableCameraLocal> ();
		paused = false;
		inGamePause = GameObject.FindGameObjectWithTag("inGamePause");
	}
	
	// Update is called once per frame
	void Update () {
		if(!paused && (Input.GetButtonDown("P1_Pause") || Input.GetButtonDown("P2_Pause")))
			pausedOnLocal();
		else if(paused)
		{
			if(Input.GetButtonDown("P1_Pause") || Input.GetButtonDown("P2_Pause")) 
				pausedOffLocal();

			if(Input.GetButtonDown("P1_Quit") || Input.GetButtonDown("P2_Quit"))
				quitToMenu();
		}
	}

	void quitToMenu()
	{
		Vector3 camStartingPos = GameObject.Find ("CamStartingPos").transform.position;
		cam.transform.position = camStartingPos;
		p1script.gameObject.transform.position = camStartingPos;
		p2script.gameObject.transform.position = camStartingPos;
		cam.chaseLevelLocal();

		Destroy (p1script.gameObject);
		Destroy (p2script.gameObject);
		menu.enableEverything ();
	}

	void pausedOnLocal()
	{
		paused = true;
		//boxdisplay.setImageLocal (pausedImage);
		//boxdisplay.renderer.enabled = true;
		showPauseMenu ();
		p1script.pauseAnimation (true);
		p2script.pauseAnimation (true);
		p1script.enabled = false;
		p2script.enabled = false;
		freezescript.enabled = false;
		rewindscript.enabled = false;
		rigidbody2D.velocity = Vector3.zero;
	}
	void pausedOffLocal()
	{
		paused = false;
		//boxdisplay.renderer.enabled = false;
		hidePauseMenu ();
		p1script.pauseAnimation (false);
		p2script.pauseAnimation (false);
		p1script.enabled = true;
		p2script.enabled = true;
		freezescript.enabled = true;
		rewindscript.enabled = true;
	}

	void showPauseMenu()
	{
		inGamePause.renderer.material.color = new Color (1, 1, 1, 1);
	}
	void hidePauseMenu()
	{
		inGamePause.renderer.material.color = new Color (0,0,0,0);
	}
}
