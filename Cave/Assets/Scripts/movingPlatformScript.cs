﻿using UnityEngine;
using System.Collections;

public class movingPlatformScript : FreezableObject {

	int frames;

	public Vector2 speedUp;
	public Vector2 speedDown;
	public int distance;

	bool up;

	Vector2 oldVelocity;

	bool gameStarted;

	// Use this for initialization
	void Start () {
		frames = 0;
		if(networkView.isMine) rigidbody2D.velocity = speedUp;
		//rigidbody2D.gravityScale = 0;
		up = true;
		gameStarted = false;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (networkView.isMine) {
			if (!frozen) {
				if (unFrozenLastFrame) {
					rigidbody2D.velocity = savedVel;
					unFrozenLastFrame = false;
				}
				frames++;
				if (frames >= distance)
					flipVelocity ();
			} else 	
				rigidbody2D.velocity = new Vector2 (0, 0);				
			}
	}

	void flipVelocity()
	{
		if (up) {rigidbody2D.velocity = speedDown; up = false;}
		else {rigidbody2D.velocity = speedUp; up = true;}
		frames = 0;
	}

//	[RPC]
//	void StartGame()
//	{
//		gameStarted = true;
//	}

	void OnDisable()
	{
		oldVelocity = rigidbody2D.velocity;
		rigidbody2D.velocity = new Vector2 (0, 0);
	}
	void OnEnable()
	{
		rigidbody2D.velocity = oldVelocity;
	}

//	void OnGUI() {
//		GUI.Label (new Rect(0,50,500,500),frozen.ToString());
//
//	}

//	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
//		Vector3 velocity = Vector3.zero; //grabbed - dunno if it should be set to false...
//		if (stream.isWriting) {
//			velocity = rigidbody2D.velocity;
//			//stream.Serialize(ref bar);
//			stream.Serialize(ref velocity);
//			
//		} else { 
//			stream.Serialize(ref velocity);
//			rigidbody2D.velocity = velocity;
//		}
//	}
}
