﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameManager : MonoBehaviour {

	public GameObject[] levels;
	public GameObject player1;
	public GameObject player2;
	public GameObject p1box;
	public GameObject p2box;
	public GameObject networkManager;
	private GameObject gameEndName;
	private GameObject waitingForP2;
	private int currentLevel = 0;
	private bool waitingScreenNotFound;
	private List<GameObject> shadows;
	private GameObject[] shadowSpawns;
	private GameObject p2;
	private GameObject p1;
	private bool unShadowing = false;

	private bool networked, local;
	private Player1ControlLocal p1scriptlocal;
	private Player2ControlLocal p2scriptlocal;
	private Player1Control p1script;
	private Player2Control p2script;
	private RewindScriptLocal rewindscriptlocal;
	private RewindScript rewindscript;
	private FreezeScript freezescript;
	private FreezeScriptLocal freezescriptlocal;
	private float percentShadowed = 1.0f;

	//used for the temporary teleporting feature
	private ToggleableCameraLocal cam;

	//holds all the positions of the levels
	ArrayList levelPositions = new ArrayList ();

	//only set up for local right now
	private Color p1StartColor, p2StartColor;
	public Color endColor = new Color(0,0,0,.6f); //black

	P1BoxDisplayLocal p1boxdisplaylocal;
	P2BoxDisplayLocal p2boxdisplaylocal;
	P1BoxDisplay p1boxdisplay;
	P2BoxDisplay p2boxdisplay;

	//stuff that will be displayed above players' heads
	public Sprite restartImage;
	public Sprite[] convoImages;

	public static bool restart = false;

	private bool tryToStart;
	private bool inIntro;
	
	bool serverRestarting = false;
	bool clientRestarting = false;

	// Use this for initialization
	void Start () {

		//Initialize variables
		shadows = new List<GameObject> ();
		networked = local = false;
		cam = (ToggleableCameraLocal)FindObjectOfType (typeof(ToggleableCameraLocal));
		for(int i=0; i<levels.Length; i++)
			levelPositions.Add(GameObject.FindGameObjectWithTag("Level" + i).transform.position);
		waitingScreenNotFound = true;
		waitingForP2 = GameObject.FindGameObjectWithTag("waitingOnP2");
		shadowSpawns = GameObject.FindGameObjectsWithTag ("shadowSpawn");
		tryToStart = false;
		inIntro = false;
		gameEndName = GameObject.FindGameObjectWithTag("gameNameEnd");
	}

	void reset()
	{
		FreezableObject[] doors, flipDoors;
		SwitchScript[] switches;
		SwitchScriptLocal[] switcheslocal;
		doors = flipDoors = null;
		switches = null;
		switcheslocal = null;
		if(networked) {
			doors = 	(FreezableObject[])FindObjectsOfType(typeof(DoorScript));
			flipDoors = (FreezableObject[])FindObjectsOfType(typeof(FlipDoorScript));
			switches = 	(SwitchScript[])FindObjectsOfType(typeof(SwitchScript));
			for(int i=0; i<switches.Length; i++)
				switches[i].reset();
		}
		else if(local)
		{
			doors = 	(FreezableObject[])FindObjectsOfType(typeof(DoorScriptLocal));
			flipDoors = (FreezableObject[])FindObjectsOfType(typeof(FlipDoorScriptLocal));
			switcheslocal = (SwitchScriptLocal[])FindObjectsOfType(typeof(SwitchScriptLocal));
			for(int i=0; i<switcheslocal.Length; i++)
				switcheslocal[i].reset();
		}
		for(int i=0; i<doors.Length; i++)
			doors[i].reset();
		for(int i=0; i<flipDoors.Length; i++)
			flipDoors[i].reset();

	}

	public void startFromLevel(int level)
	{
		//call reset in the case that the player quit and went back to the main menu and 
		//starts game again -- we want the doors and such to be reset in that case
		reset ();
		currentLevel = level;
		//currentLevel = level = 0;
		if(level == 0)
		{
			StartCoroutine("introConvo");
		}

		//fadePlayer ();

		//save level (only necessary because of the ability to jump between levels)
		if(networked)  
			PlayerPrefs.SetInt ("networkedLevel", currentLevel);
		else if(local) 
			PlayerPrefs.SetInt ("localLevel", currentLevel);

		GameObject obj = GameObject.FindGameObjectWithTag("Level" + level);
		obj.layer = LayerMask.NameToLayer ("Default");
		Vector3 pos = (Vector3) levelPositions[level];
		
		if(Network.isServer) //is network play
		{
			obj.layer = LayerMask.NameToLayer ("Default");
			p1script.networkView.RPC("setPosition", RPCMode.All, pos - new Vector3(2f,0f,0f));
			p2script.networkView.RPC("setPosition", RPCMode.All, pos);
		}
		else if(local)//is local play
		{
			obj.layer = LayerMask.NameToLayer ("Default");
			p1scriptlocal.setPositionLocal(pos - new Vector3(2f, 0f, 0f));
			p2scriptlocal.setPositionLocal(pos);
		}
	}

	// Update is called once per frame
	void Update () {

		if (tryToStart) {
			try {
				p1script.enabled = true;
				p2boxdisplay.enabled = true;
				tryToStart = false;
				networkView.RPC("setInIntro", RPCMode.All, false);
				p2boxdisplay.networkView.RPC("initialize", RPCMode.All, null);
				startFromLevel(currentLevel);
			} catch {}
		}

		//reset level if both players are holding down the reset button

		if (unShadowing) {
			gameEndName.renderer.material.color = new Color(0.0f,0.0f,0.0f,percentShadowed);

		}

		if(!inIntro)
		{
			//STUFF FOR PLAYERS HOLDING RESTART BUTTON
			if(local)
			{
				bool restartP1 = Input.GetButton ("P1_Restart");
				bool restartP2 = Input.GetButton ("P2_Restart");

				if(Input.GetButtonDown("P1_Restart"))
				{
					p1boxdisplaylocal.setImageLocal(restartImage);
					p1boxdisplaylocal.renderer.enabled = true;
				}
				else if(Input.GetButtonUp("P1_Restart"))
					p1boxdisplaylocal.renderer.enabled = false;

				if(Input.GetButtonDown("P2_Restart"))
				{
					p2boxdisplaylocal.setImageLocal(restartImage);
					p2boxdisplaylocal.renderer.enabled = true;
				}
				else if(Input.GetButtonUp("P2_Restart"))
					p2boxdisplaylocal.renderer.enabled = false;

				if(restartP1 && restartP2)
				{
					p1boxdisplaylocal.renderer.enabled = false;
					p2boxdisplaylocal.renderer.enabled = false;
					startFromLevel(currentLevel);
				}
			}
			else if(networked)//networked
			{
				bool serverDown = false;
				bool serverUp = false;
				bool clientDown = false;
				bool clientUp = false;


				if(Network.isServer)
				{
					//serverRestarting = Input.GetButton("P1_Restart");
					serverDown = Input.GetButtonDown("P1_Restart");
					serverUp = Input.GetButtonUp("P1_Restart");
					if(serverDown)
					{
						p1boxdisplay.networkView.RPC("setVisible",RPCMode.All, true);
						p1boxdisplay.networkView.RPC("setImage", RPCMode.All, "TryingToRestart"); 
					}
					else if(serverUp)
						p1boxdisplay.networkView.RPC("setVisible",RPCMode.All, false);
				}
				else {
					//clientRestarting = Input.GetButton("P1_Restart");
					clientDown = Input.GetButtonDown("P1_Restart");
					clientUp = Input.GetButtonUp("P1_Restart");
					if(clientDown)
					{
						p2boxdisplay.networkView.RPC("setVisible",RPCMode.All, true);
						p2boxdisplay.networkView.RPC("setImage", RPCMode.All, "TryingToRestart");
					}
					else if(clientUp)
						p2boxdisplay.networkView.RPC("setVisible",RPCMode.All, false);
				}
				//Debug.Log(serverRestarting.ToString() + clientRestarting.ToString());
				//if(serverRestarting && clientRestarting)
				if(Network.isServer)
				{
					if (restart)
					{
						p1boxdisplay.networkView.RPC("setVisible", RPCMode.All, false);
						p2boxdisplay.networkView.RPC("setVisible", RPCMode.All, false);
						startFromLevel(currentLevel);
					}
				}
			}
		}

		//Teleporting players to different levels, for both debugging and playtesting purposes 
		if(Input.GetKeyDown("1"))
			startFromLevel(2); //actually level 1 (it's higher because of intro and transition levels in array, as well
		if(Input.GetKeyDown("2"))
			startFromLevel(4); //actually level 2
		if(Input.GetKeyDown("3"))
			startFromLevel(6);
		if(Input.GetKeyDown("4"))
			startFromLevel(9);
		if(Input.GetKeyDown("5"))
			startFromLevel(12);
		if(Input.GetKeyDown("6"))
			startFromLevel(15);

		//skip the intro for debugging purposes
		if(Input.GetKeyDown("s"))
		{
			if(networked && !Network.isServer) return;
		  
			StopCoroutine("introConvo");
			enablePlayers();
			if(networked)
				networkView.RPC("setInIntro", RPCMode.All, false);
			else
				inIntro = false;
			if(local)
			{
				p1boxdisplaylocal.renderer.enabled = false;
				p2boxdisplaylocal.renderer.enabled = false;
			}
			else{
				p1boxdisplay.networkView.RPC("setVisible", RPCMode.All, false);
				p2boxdisplay.networkView.RPC("setVisible", RPCMode.All, false);
			}
		}
	}

	[RPC]
	void setInIntro(bool b)
	{
		inIntro = b;
	}

	public void enableNetworkedPlay()
	{
		networked = true;
		networkManager.SetActive (true);
	}

	public void enableLocalPlay()
	{
		local = true;
		//instantiate two players normally
		Instantiate(player1, Vector3.zero, Quaternion.identity);
		Instantiate(player2, Vector3.zero, Quaternion.identity);
		Instantiate(p1box, Vector3.zero, Quaternion.identity);
		Instantiate(p2box, Vector3.zero, Quaternion.identity);
		//Debug.Log (Instantiate(player1, Vector3.zero, Quaternion.identity));

		startGame ();

		//RUN THROUGH ALL SCRIPTS IN SCENE AND DISABLE NETWORK SCRIPTS AND ENABLE LOCAL ONES
		MonoBehaviour[] scripts = (MonoBehaviour[])FindObjectsOfType(typeof(MonoBehaviour));
		foreach(MonoBehaviour s in scripts)
		{

			if(s.ToString().Contains("(PausedLocal)") || s.ToString().Contains("(P2BoxDisplayLocal)") || s.ToString().Contains("(P1BoxDisplayLocal)") || s.ToString().Contains("(PressurePlateScriptLocal)") || s.ToString().Contains("(DoorScriptLocal)") || s.ToString().Contains("(ClockHandScriptLocal)") || s.ToString().Contains("(SwitchScriptLocal)") || s.ToString().Contains("(FlipDoorScriptLocal)") || s.ToString().Contains("(DetectionLocal)") || s.ToString().Contains("(FreezeScriptLocal)") || s.ToString().Contains("(PaperScriptLocal)") || s.ToString().Contains("(Player1ControlLocal)") || s.ToString().Contains("(Player2ControlLocal)") || s.ToString().Contains("(RewindScriptLocal)") || s.ToString().Contains("(ToggleableCameraLocal)"))
				s.enabled = true;
			
			if(s.ToString().Contains("(P1Paused)") || s.ToString().Contains("(P2Paused)") || s.ToString().Contains("(P2BoxDisplay)") || s.ToString().Contains("(P1BoxDisplay)") || s.ToString().Contains("(PressurePlateScript)") || s.ToString().Contains("(DoorScript)") || s.ToString().Contains("(ClockHandScript)") || s.ToString().Contains("(SwitchScript)") || s.ToString().Contains("(FlipDoorScript)") || s.ToString().Contains("(Detection)") || s.ToString().Contains("(FreezeScript)") || s.ToString().Contains("(PaperScript)") || s.ToString().Contains("(Player1Control)") || s.ToString().Contains("(Player2Control)") || s.ToString().Contains("(RewindScript)") || s.ToString().Contains("(ToggleableCamera)"))
				s.enabled = false;
		}

		//StartCoroutine("introConvo");
	}

	public void findPlayerScripts()
	{
		if(local)
		{
			p1scriptlocal = (Player1ControlLocal)FindObjectOfType (typeof(Player1ControlLocal));
			p2scriptlocal = (Player2ControlLocal)FindObjectOfType (typeof(Player2ControlLocal));
			p1StartColor = p1scriptlocal.gameObject.renderer.material.color;
			p2StartColor = p2scriptlocal.gameObject.renderer.material.color;
			p1boxdisplaylocal = (P1BoxDisplayLocal)FindObjectOfType (typeof(P1BoxDisplayLocal));
			p2boxdisplaylocal = (P2BoxDisplayLocal)FindObjectOfType (typeof(P2BoxDisplayLocal));
			freezescriptlocal = (FreezeScriptLocal)FindObjectOfType(typeof(FreezeScriptLocal));
			rewindscriptlocal = (RewindScriptLocal)FindObjectOfType(typeof(RewindScriptLocal));
		}
		else
		{
			p1script = (Player1Control)FindObjectOfType (typeof(Player1Control));
			//p2script = (Player2Control)FindObjectOfType (typeof(Player2Control));
			p1boxdisplay = (P1BoxDisplay)FindObjectOfType (typeof(P1BoxDisplay));
			//p2boxdisplay = (P2BoxDisplay)FindObjectOfType (typeof(P2BoxDisplay));
			//freezescript = (FreezeScript)FindObjectOfType(typeof(FreezeScript));
			rewindscript = (RewindScript)FindObjectOfType(typeof(RewindScript));
		}
	}

	//called from player2control 
	public void getPlayer2(NetworkViewID id)
	{
		p2 = NetworkView.Find(id).gameObject;
		p2script = p2.GetComponent<Player2Control> ();
		freezescript = p2.GetComponent<FreezeScript> ();
	}

	public void getP2BoxDisplay(NetworkViewID id)
	{
		GameObject p2Box = NetworkView.Find (id).gameObject;
		p2boxdisplay = p2Box.GetComponent<P2BoxDisplay> ();
	}

	public void end() {

		for (int i = 0; i < shadowSpawns.Length; i++) {
			shadows.Add((GameObject)Instantiate (Resources.Load ("Shadow")));
			shadows[i].transform.position = shadowSpawns[i].transform.position;
		}

		p1scriptlocal.startShadowfication(5);
		p2scriptlocal.startShadowfication (5);
	}

	public void end2() {
		shadows.Add((GameObject)Instantiate (Resources.Load ("Shadow")));
		shadows [shadows.Count - 1].transform.position = p1scriptlocal.gameObject.transform.position;

		shadows.Add((GameObject)Instantiate (Resources.Load ("Shadow")));
		shadows [shadows.Count - 1].transform.position = p2scriptlocal.gameObject.transform.position;

		Destroy (p1scriptlocal.gameObject);
		Destroy (p2scriptlocal.gameObject);

		unShadowName ();


		//player1.SetActive (false);
		//player2.SetActive (false);
	}

	private void unShadowName() {
		float duration = 5;
		StartCoroutine("unShadow");
	}

	IEnumerator unShadow()
	{
		unShadowing = true;
		float timer = 0.0f;
		while(timer < 5) 
		{
			timer += Time.deltaTime;
			percentShadowed =  (timer / 5.0f);
			yield return null;
		}
	}

	[RPC]
	void bothPlayersConnected()
	{
		if (Network.isClient)
		{
			Network.Instantiate (player2, new Vector3 (5.8f, -3f, 0f), Quaternion.identity, 0);
			Network.Instantiate (p2box, new Vector3 (5.8f, -3f, 0f), Quaternion.identity, 0);
		}
		else if (Network.isServer) {
			Network.Instantiate (player1, new Vector3 (6.1f, -3f, 0f), Quaternion.identity, 0);
			Network.Instantiate (p1box, new Vector3 (5.8f, -3f, 0f), Quaternion.identity, 0);
			waitingForP2.SetActive(false);
		}
		if(Network.isServer) {
			startGame ();
		}
	}	

	public void startGame()
	{
		for(int i=0; i<levels.Length; i++)
			enableLevel(i);

		findPlayerScripts ();

		//IT'S FINDING PLAYER SCRIPTS, JUST NOT BOX DISPLAYS

		if(local)
		{
			p1boxdisplaylocal.initializeLocal();
			p2boxdisplaylocal.initializeLocal();
			startFromLevel(currentLevel); //is set by the MenuController
		}
		else if(networked)
		{
			//p1boxdisplay.initialize();
			p1boxdisplay.networkView.RPC("initialize", RPCMode.All, null);
			//p2boxdisplay.networkView.RPC("initialize", RPCMode.All, null);
			//startFromLevel(currentLevel); //is set by the MenuController
			tryToStart = true;
		}

	}

	public void setLevel(int level)
	{
		currentLevel = level;
	}

	void  enableLevel(int l)
	{
		levels [l].SetActive (true);
	}

	void disableLevel(int l)
	{
		levels [l].SetActive (false);
	}

	public void loadNextLevel()
	{
		currentLevel++;

		//save game
		if(networked)	PlayerPrefs.SetInt("networkedLevel", currentLevel);
		else if(local)	PlayerPrefs.SetInt("localLevel", currentLevel);

		//fadePlayer ();
	}	

//	void fadePlayer()
//	{
//		//THIS ASSUMES ALL THE RGB VALUES OF STARTCOLOR ARE HIGHER THAN RGB VALUES OF ENDCOLOR
//		float rDifference = p1StartColor.r - endColor.r;
//		float gDifference = p1StartColor.g - endColor.g;
//		float bDifference = p1StartColor.b - endColor.b;
//		float aDifference = p1StartColor.a - endColor.a;
//		float percent = (float)currentLevel / levels.Length;
//		//players are "percent" of the way through the game, so subtract percent*colorDifference from startColor
//		p1scriptlocal.gameObject.renderer.material.color = new Color (p1StartColor.r - percent * rDifference, 
//              p1StartColor.g - percent * gDifference, p1StartColor.b - percent * bDifference, p1StartColor.a - percent * aDifference);
//
//		//for player 2
//		rDifference = p2StartColor.r - endColor.r;
//		gDifference = p2StartColor.g - endColor.g;
//		bDifference = p2StartColor.b - endColor.b;
//		aDifference = p2StartColor.a - endColor.a;
//		p2scriptlocal.gameObject.renderer.material.color = new Color (p2StartColor.r - percent * rDifference, 
//              p2StartColor.g - percent * gDifference, p2StartColor.b - percent * bDifference, p2StartColor.a - percent * aDifference);
//	}

	public bool getLocal()
	{
		return local;
	}
	public bool getNetworked()
	{
		return networked;
	}

	void disablePlayers()
	{
		if(local)
		{
			p1scriptlocal.enabled = false;
			p2scriptlocal.enabled = false;
			p1scriptlocal.setVelocityLocal(Vector3.zero);
			p2scriptlocal.setVelocityLocal(Vector3.zero);
		}
		else{
			
			p1script.networkView.RPC("setVelocity", RPCMode.All, Vector3.zero);
			p2script.networkView.RPC("setVelocity", RPCMode.All, Vector3.zero);
			p1script.enabled = false;
			//p2script.enabled = false;
			p2script.networkView.RPC("disable", RPCMode.All, null);
		}
	}



	void enablePlayers()
	{
		if(local)
		{
			p1scriptlocal.enabled = true;
			p2scriptlocal.enabled = true;
		}
		else
		{
			p1script.enabled = true;
			//p2script.enabled = true;
			p2script.networkView.RPC("enable", RPCMode.All, null);
		}
	}

	IEnumerator introConvo()
	{
		if(networked && Network.isClient) yield break;

		if(networked)
			networkView.RPC("setInIntro", RPCMode.All, true);
		else
			inIntro = true;

		//.2 SECOND WAIT PURELY FOR ALLOWING THE SCRIPTS TO LOAD UP I GUESS. 
		float timer = 0;
		while(timer < .2f) 
		{
			timer += Time.deltaTime;
			yield return null;
		}

		disablePlayers();

		timer = 0;
		while(timer < 3f) 
		{
			timer += Time.deltaTime;
			yield return null;
		}

		
		if(local)
		{
			//1
			p1boxdisplaylocal.renderer.enabled = true;
			p1boxdisplaylocal.setImageLocal(convoImages[0]);
			timer = 0;
			while(timer < 2.7f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplaylocal.renderer.enabled = false;
			//2
			p2boxdisplaylocal.renderer.enabled = true;
			p2boxdisplaylocal.setImageLocal(convoImages[1]);
			timer = 0;
			while(timer < 2.2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplaylocal.renderer.enabled = false;
			//3
			p1boxdisplaylocal.renderer.enabled = true;
			p1boxdisplaylocal.setImageLocal(convoImages[2]);
			timer = 0;
			while(timer < 2.5f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplaylocal.renderer.enabled = false;
			//4
			p2boxdisplaylocal.renderer.enabled = true;
			p2boxdisplaylocal.setImageLocal(convoImages[3]);
			timer = 0;
			while(timer < 1.5f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplaylocal.renderer.enabled = false;
			//5
			p1boxdisplaylocal.renderer.enabled = true;
			p1boxdisplaylocal.setImageLocal(convoImages[4]);
			timer = 0;
			while(timer < 2.2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplaylocal.renderer.enabled = false;
			//6
			p2boxdisplaylocal.renderer.enabled = true;
			p2boxdisplaylocal.setImageLocal(convoImages[5]);
			timer = 0;
			while(timer < 3.8f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplaylocal.renderer.enabled = false;
			//7
			p1boxdisplaylocal.renderer.enabled = true;
			p1boxdisplaylocal.setImageLocal(convoImages[6]);
			timer = 0;
			while(timer < 2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplaylocal.renderer.enabled = false;
			//8
			p2boxdisplaylocal.renderer.enabled = true;
			p2boxdisplaylocal.setImageLocal(convoImages[7]);
			timer = 0;
			while(timer < 3.4f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplaylocal.renderer.enabled = false;
			//9
			p1boxdisplaylocal.renderer.enabled = true;
			p1boxdisplaylocal.setImageLocal(convoImages[8]);
			timer = 0;
			while(timer < 2.8f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplaylocal.renderer.enabled = false;
			//10
			p2boxdisplaylocal.renderer.enabled = true;
			p2boxdisplaylocal.setImageLocal(convoImages[9]);
			timer = 0;
			while(timer < 2.6f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplaylocal.renderer.enabled = false;
			//11
			p1boxdisplaylocal.renderer.enabled = true;
			p1boxdisplaylocal.setImageLocal(convoImages[10]);
			timer = 0;
			while(timer < 3.2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplaylocal.renderer.enabled = false;
			//12
			p2boxdisplaylocal.renderer.enabled = true;
			p2boxdisplaylocal.setImageLocal(convoImages[11]);
			timer = 0;
			while(timer < 3f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplaylocal.renderer.enabled = false;
		}
		else //networked
		{
			//1
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p1boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo1");
			timer = 0;
			while(timer < 2.7f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//2
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p2boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo2");
			timer = 0;
			while(timer < 2.2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//3
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p1boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo3");
			timer = 0;
			while(timer < 2.5f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//4
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p2boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo4");
			timer = 0;
			while(timer < 1.5f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//5
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p1boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo5");
			timer = 0;
			while(timer < 2.2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//6
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p2boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo6");
			timer = 0;
			while(timer < 3.8f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//7
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p1boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo7");
			timer = 0;
			while(timer < 2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//8
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p2boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo8");
			timer = 0;
			while(timer < 3.4f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//9
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p1boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo9");
			timer = 0;
			while(timer < 2.8f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//10
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p2boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo10");
			timer = 0;
			while(timer < 2.6f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//11
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p1boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo11");
			timer = 0;
			while(timer < 3.2f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p1boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
			//12
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,true);
			p2boxdisplay.networkView.RPC("setImage", RPCMode.All,"convo12");
			timer = 0;
			while(timer < 3f) 
			{
				timer += Time.deltaTime;
				yield return null;
			}
			p2boxdisplay.networkView.RPC("setVisible", RPCMode.All,false);
		}

		timer = 0;
		while(timer < 1f) 
		{
			timer += Time.deltaTime;
			yield return null;
		}
		if(networked)
			networkView.RPC("setInIntro", RPCMode.All, false);
		else
			inIntro = false;
		enablePlayers ();
	}

//	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
//		//fires up when data to be sent.
//		bool server = false;
//		bool client = false;
//		if (stream.isWriting) { //we want to send data
//			server = serverRestarting;
//			client = clientRestarting;
//			stream.Serialize(ref server);
//			stream.Serialize(ref client);
//
//		} else { //we deal with received data
//			stream.Serialize(ref server);
//			serverRestarting = server;
//			stream.Serialize(ref client);
//			clientRestarting = client;
//		}
//	}
}
