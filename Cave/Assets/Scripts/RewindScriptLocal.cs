﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class RewindScriptLocal : FreezableObject {

	public Texture2D emptyTex;
	public Texture2D fullTex;
	public int extraFrames;
	public int maxFrames;

	//Bar stuff
	private float percentFull; //current progress of distance player allowed to travel
	private Vector2 pos;   
	private Vector2 size;
	private bool barFull;

	private int maxElapsedFrames;
	private int numFramesElapsed;
	private float xPosLastFrame, yPosLastFrame;
	private Stack horizontalTransforms = new Stack();
	private Stack verticalTransforms = new Stack();
	private List<Vector2> velocityStack = new List<Vector2>();
	private float initialPositionX, initialPositionY;
	private bool rewind;
	private Vector2 mBoxSize;
	private bool wasJustRewinding = false;
	private bool powerEnabled = false;

	private ClockHandScriptLocal clockHandScript;
	private Player1ControlLocal player1script;
	private Animator p1Animator;

	//private float p1Color;
	//private SpriteRenderer p1SpriteRenderer;

	// Use this for initialization
	void Start () {
		size = new Vector2(300,20);
		pos = new Vector2 (20, 40);
		initialPositionX = transform.position.x;
		initialPositionY = transform.position.y;
		xPosLastFrame = transform.position.x - 20.0f;
		yPosLastFrame = transform.position.y - 20.0f;
		numFramesElapsed = 0;
		mBoxSize = (collider2D as BoxCollider2D).size;
		barFull = false;
		maxElapsedFrames = maxFrames * (extraFrames + 1);
		//clockHandScript = (ClockHandScriptLocal)FindObjectOfType (typeof(ClockHandScriptLocal));
		player1script = (Player1ControlLocal)FindObjectOfType (typeof(Player1ControlLocal));
		p1Animator = gameObject.GetComponent<Animator>();
		//p1Color = 0.0f;
		//p1SpriteRenderer = this.GetComponent<SpriteRenderer> ();
	}

	void FixedUpdate () { 

		percentFull = numFramesElapsed/(float)maxElapsedFrames;
		//clockHandScript.setHand (percentFull);
		//p1Color = 1.0f - 0.92f * percentFull;
		//p1SpriteRenderer.color = new Vector4(p1Color, p1Color, p1Color, 1);

		rewind = Input.GetButton("Rewind");
		if (powerEnabled && !frozen) {
			if (rewind && !player1script.getGrabbed()) {
				if(!movementBarNotFull()) //call barNotFilled when bar was filled and rewind pressed
					barNotFilledLocal();
				Rewind();
			} else {
				if (wasJustRewinding) {
					rigidbody2D.velocity = -rigidbody2D.velocity;
					wasJustRewinding = false;
					setKinematicLocal (false);
				}

				if(!movementBarNotFull()) 
					barFilledLocal();
				else {
					recordPositionIfMoved ();
				}
			}
		}
	}

	void Rewind()
	{
		setKinematicLocal (true);
		wasJustRewinding = true;
		if (horizontalTransforms.Count > 0) {
			//transform.position = Vector2.Lerp(transform.position, new Vector2 ((float)horizontalTransforms.Pop (), (float)verticalTransforms.Pop ()), Time.deltaTime * speedFactor);
			transform.position = new Vector2 ((float)horizontalTransforms.Pop (), (float)verticalTransforms.Pop ());
			//rigidbody2D.velocity = velocityStack[velocityStack.Count - 1] * -1;
			velocityStack.RemoveAt(velocityStack.Count - 1);
			numFramesElapsed--;
		} else {
			rigidbody2D.velocity = new Vector2 (0, 0);
			transform.position = new Vector2(initialPositionX, initialPositionY);
		}
	}

	void recordPositionIfMoved ()
	{
		//if i decrease 0.02, increase max frames
		if (diffGTX (transform.position.x, xPosLastFrame, 0.02) || diffGTX (transform.position.y, yPosLastFrame, 0.02)) {

			pushExtraFrames();

			horizontalTransforms.Push (gameObject.transform.position.x);
			verticalTransforms.Push (gameObject.transform.position.y);
			velocityStack.Add(gameObject.rigidbody2D.velocity);
			numFramesElapsed += (1 + extraFrames);
			//numFramesElapsed += 1;
			xPosLastFrame = transform.position.x;
			yPosLastFrame = transform.position.y;
		}
	}

	void pushExtraFrames ()
	{
		float p1 = xPosLastFrame;
		float p2 = yPosLastFrame;
		float q1 = transform.position.x;
		float q2 = transform.position.y;
		//Vector2 P = new Vector2(p1, p2);
		//Vector2 Q = new Vector2(q1, q2);

		int divisor = extraFrames + 1;
		for (int i = 1; i < extraFrames + 1; i++) {
			float f = i/divisor;
			Vector2 newPoint = new Vector2(f*p1 + (1-f)*q1, f*p2 + (1-f)*q2);

			horizontalTransforms.Push(newPoint.x);
			verticalTransforms.Push(newPoint.y);
			velocityStack.Add(gameObject.rigidbody2D.velocity);
		}
	}

	bool diffGTX (float num1, float num2, double x)
	{
		return (Mathf.Abs(num1-num2) > x);
	}
	
	public bool movementBarNotFull() {
		return (numFramesElapsed < maxElapsedFrames);
	}

//	void OnGUI() {
//
//		//GUI.Label (new Rect(0,50,500,500), rigidbody2D.isKinematic.ToString());
//
//		//draw the background:
//		GUI.BeginGroup(new Rect(pos.x, pos.y, size.x, size.y));
//		GUI.Box(new Rect(0,0, size.x, size.y), emptyTex);
//		
//		//draw the filled-in part:
//		GUI.BeginGroup(new Rect(0,0, size.x * percentFull, size.y));
//		GUI.Box(new Rect(0,0, size.x, size.y), fullTex);
//		GUI.EndGroup();
//		GUI.EndGroup();
//	}

	public bool getBarFull() {
		return barFull;
	}

	public float getPercentFull() {
		if (powerEnabled)
			return percentFull;
		else //otherwise, percentFull is irrelevant;
			return 0.0f;
	}

	void setKinematicLocal(bool k)
	{
		rigidbody2D.isKinematic = k;
		if (k)
			p1Animator.enabled = false;
		else
			p1Animator.enabled = true;
	}


	void barFilledLocal()
	{
		rigidbody2D.velocity = new Vector2 (0, 0);
		barFull = true;
		rigidbody2D.isKinematic = true;
		p1Animator.enabled = false;
	}

	void barNotFilledLocal()
	{
		barFull = false;
		rigidbody2D.isKinematic = false;
		p1Animator.enabled = true;
	}

	public void resetRewindLocal()
	{
		numFramesElapsed = 0;
		barFull = false;
		rigidbody2D.isKinematic = false;
		p1Animator.enabled = true;
		horizontalTransforms.Clear ();
		verticalTransforms.Clear ();
		velocityStack.Clear ();
		initialPositionX = transform.position.x;
		initialPositionY = transform.position.y;
	}

	public void setPowerEnabledLocal(bool b)
	{
		powerEnabled = b;
	}

	public bool getPowerEnabled()
	{
		return powerEnabled;
	}
}
