﻿using UnityEngine;
using System.Collections;

public class PaperScript : MonoBehaviour {
	
	public Sprite paperImage;
	public GameObject paperDisplay;
	private SpriteRenderer r;
	
	bool p1on, p2on;
	
	// Use this for initialization
	void Start () {
		p1on = p2on = false;
		
		//center the paperDisplay gameObject on the screen
		paperDisplay.transform.position = new Vector2 (0, 0);
		//set the sprite image for the paperDisplay object
		r = paperDisplay.GetComponent<SpriteRenderer> ();
		r.sprite = paperImage;
		
		//make the paper display invisible to start
		paperDisplay.renderer.enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter2D(Collider2D col)
	{
		if(!enabled) return;

		if(Network.isServer && col.gameObject.tag == "P1")
		{
			r.sprite = paperImage;
			paperDisplay.renderer.enabled = true;
			p1on = true;
		}
		else if(Network.isClient && col.gameObject.tag == "P2")
		{
			r.sprite = paperImage;
			paperDisplay.renderer.enabled = true;
			p2on = true;
		}
	}
	
	void OnTriggerExit2D(Collider2D col)
	{
		if(!enabled) return;

		if(Network.isServer && col.gameObject.tag == "P1")
			p1on = false;
		else if(Network.isClient && col.gameObject.tag == "P2")
			p2on = false;
		if(!p1on && !p2on)
			paperDisplay.renderer.enabled = false;
	}
}
