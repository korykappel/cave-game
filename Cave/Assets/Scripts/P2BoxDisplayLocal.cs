﻿using UnityEngine;
using System.Collections;

public class P2BoxDisplayLocal: MonoBehaviour {
	
	SpriteRenderer renderer;
	Vector3 restartPosition = new Vector3(0,1.5f,0);
	Vector3 pausedPosition = new Vector3(0,2.2f,0);
	Vector3 convoPosition = new Vector3(2.1f, 2.5f, 0);

	Transform p2;
	
	// Use this for initialization
	void Start () {
		
		renderer = GetComponent<SpriteRenderer> ();
		renderer.enabled = false;
	}

	// Update is called once per frame
	void Update () {
	}
	
	public void setImageLocal(Sprite image)
	{
		GetComponent<SpriteRenderer> ().sprite = image;
		if(image.ToString().Contains("TryingToRestart"))
			transform.position = p2.position + restartPosition;
		else if(image.ToString().Contains("Paused"))
			transform.position = p2.position + pausedPosition;
		else if(image.ToString().Contains("Convo"))
			transform.position = p2.position + convoPosition;
	}
	
	public void initializeLocal()
	{
		p2 = GameObject.Find ("Player2(Clone)").transform;
		transform.position = p2.position + convoPosition;
		transform.parent = p2;
	}
	
	//IEnumerator designates this function as a coroutine, which can return after one frame and pick up execution in the next
	public IEnumerator displayForXSeconds(float seconds)
	{
		renderer.enabled = true;
		float timer = 0;
		while(timer < seconds)
		{
			timer += Time.deltaTime;
			yield return null;
		}
		renderer.enabled = false;
	}
}
