﻿using UnityEngine;
using System.Collections;

public class ToggleableCamera : MonoBehaviour {

	public float dampTime = 0.15f;
	private Vector3 velocity = Vector3.zero;
	private Transform target;
	private Transform target2;
	private bool initialized;

	private GameObject player1;
	private RewindScript p1rewind;

	private bool firstChasingToggle = true;
	private bool chasingPlayers = false;
	private bool chasingLevel = true;

	private RewindScript p1Script;
	private GameObject p1;
	
	private FreezeScript p2Script;
	private GameObject p2;
	
	GameObject[] levelMarkers;

	// Use this for initialization
	void Start () {
		initialized = false;
	}

	// Update is called once per frame
	void Update () 
	{
		if (!initialized)	return;
		if (target && target2) {
			Vector3 midpoint = (target.position + target2.position) / 2.0f;
			Vector3 point = camera.WorldToViewportPoint (midpoint);
			
			Vector3 delta = midpoint - camera.ViewportToWorldPoint (new Vector3 (0.5f, 0.5f, point.z));
			Vector3 destination = transform.position + delta;
			transform.position = Vector3.SmoothDamp (transform.position, destination, ref velocity, dampTime);
		}
	}

	Transform findMarkerClosestToPlayers ()
	{
		int minIndex = 0;
		float minimumDistance = Mathf.Infinity;
		for (int i = 0; i < levelMarkers.Length; i++) {
			float distance = distanceBetweenTransforms(levelMarkers[i].transform, p1.transform);
			bool newMinimum = (distance < minimumDistance)? true : false;
			if (newMinimum) {
				minimumDistance = distance;
				minIndex = i;
			}
		}
		return levelMarkers[minIndex].transform;
	}

	float distanceBetweenTransforms (Transform t1, Transform t2) {
		return Vector2.Distance(new Vector2(t1.position.x, t1.position.y), new Vector2(t2.position.x, t2.position.y));
	}

	void gatherRequiredInformation ()
	{
		p1Script 	 = (RewindScript)FindObjectOfType(typeof(RewindScript));
		p2Script 	 = (FreezeScript)FindObjectOfType(typeof(FreezeScript));
		
		p1 			 = p1Script.gameObject;
		p2 			 = p2Script.gameObject;
		
		levelMarkers = GameObject.FindGameObjectsWithTag("Marker");
		
		//target = findMarkerClosestToPlayers();
		//target2 = target;
		target = p1.transform;
		target2 = p2.transform;

		initialized = true;
	}

		//void OnGUI() {
			//GUI.Label (new Rect(0,50,500,500), "Camera pos: " + transform.position.z.ToString());
		//}

	[RPC]
	void chasePlayers() {
		target = p1.transform;
		target2 = p2.transform;
	}

	[RPC]
	void chaseLevel() {
		target = findMarkerClosestToPlayers ();
		target2 = target;
	}

	[RPC]
	void initialize() {
		gatherRequiredInformation();
	}


}