﻿using UnityEngine;
using System.Collections;

public class P1Paused : MonoBehaviour {
	
	RewindScript rewindscript;
	Player1Control p1script;
	P1BoxDisplay boxdisplay;
	private GameObject inGamePause;
	private GameObject p2;
	private NetworkView p2view;
	//private Player2Control p2script;


	private bool paused;

	MenuController menu;
	
	ToggleableCamera cam; //needed for setting location of menu screen?
	
	// Use this for initialization
	void Start () {
		rewindscript = gameObject.GetComponent<RewindScript> ();
		p1script = gameObject.GetComponent<Player1Control> ();
		boxdisplay = (P1BoxDisplay)FindObjectOfType (typeof(P1BoxDisplay));
		menu = (MenuController)FindObjectOfType<MenuController> ();
		cam = (ToggleableCamera)FindObjectOfType<ToggleableCamera> ();
		paused = false;
		inGamePause = GameObject.FindGameObjectWithTag ("inGamePause");

	}

	// Update is called once per frame
	void Update () {
		if(!paused && Input.GetButtonDown("P1_Pause") && networkView.isMine) {
			networkView.RPC("pausedOn", RPCMode.All, null);
			showPause();
		}
		else if(paused && networkView.isMine)
		{
			if(Input.GetButtonDown("P1_Pause")) {
				networkView.RPC("pausedOff", RPCMode.All, null);
				hidePause();
			}
			if(Input.GetButtonDown("P1_Quit"))
				quitToMenu();
		}
	}


	void quitToMenu()
	{
		hidePause ();
		NetworkManager.disconnectServer();

		Vector3 camStartingPos = GameObject.Find ("CamStartingPos").transform.position;
		cam.transform.position = camStartingPos;
		p1script.gameObject.transform.position = camStartingPos;
		cam.networkView.RPC ("chaseLevel", RPCMode.All, null);
		
		Network.Destroy (p1script.gameObject);

		//some sort of RPC to server, saying player disconnected, which will then notify other player? 
		
		menu.enableEverything ();
	}

	[RPC]
	void pausedOn()
	{
		//Debug.Log ("Player 1 paused");
		paused = true;
		boxdisplay.networkView.RPC ("setImage", RPCMode.All, "Paused_small"); 
		boxdisplay.renderer.enabled = true;
		p1script.pauseAnimation (true);
		p1script.enabled = false;
		rewindscript.enabled = false;
		rigidbody2D.velocity = Vector3.zero;
	}
	[RPC]
	void pausedOff()
	{
		paused = false;
		boxdisplay.renderer.enabled = false;
		p1script.pauseAnimation (false);
		p1script.enabled = true;
		rewindscript.enabled = true;
	}
	
	void showPause() {
		inGamePause.renderer.material.color = new Color(1,1,1,1);
	}
	
	void hidePause() {
		inGamePause.renderer.material.color = new Color(1,1,1,0);
	}

}
