﻿using UnityEngine;
using System.Collections;

public class P2Paused : MonoBehaviour {
	
	FreezeScript freezeScript;
	Player2Control p2script;
	P2BoxDisplay boxdisplay;
	//Sprite pausedImage;
	private GameObject inGamePause;
	
	private bool paused;
	
	MenuController menu;
	
	ToggleableCamera cam; //needed for setting location of menu screen?
	
	// Use this for initialization
	void Start () {
		freezeScript = gameObject.GetComponent<FreezeScript> ();
		p2script = gameObject.GetComponent<Player2Control> ();
		boxdisplay = (P2BoxDisplay)FindObjectOfType (typeof(P2BoxDisplay));
		menu = (MenuController)FindObjectOfType<MenuController> ();
		cam = (ToggleableCamera)FindObjectOfType<ToggleableCamera> ();
		
		paused = false;
		inGamePause = GameObject.FindGameObjectWithTag("inGamePause");
	}

	// Update is called once per frame
	void Update () {
		if(!paused && Input.GetButtonDown("P1_Pause") && networkView.isMine)  {
			networkView.RPC("pausedOn", RPCMode.All, null);
			showPause();
		}
		else if(paused && networkView.isMine)
		{
			if(Input.GetButtonDown("P1_Pause")) {
				hidePause();
				networkView.RPC("pausedOff", RPCMode.All, null);
			}
			if(Input.GetButtonDown("P1_Quit"))
				quitToMenu();
		}
	}
	
	
	void quitToMenu()
	{
		hidePause();
		NetworkManager.disconnectServer();
		Vector3 camStartingPos = GameObject.Find ("CamStartingPos").transform.position;
		cam.transform.position = camStartingPos;
		p2script.gameObject.transform.position = camStartingPos;
		cam.networkView.RPC ("chaseLevel", RPCMode.All, null);

		Network.Destroy (p2script.gameObject);
		//some sort of RPC to server, saying player disconnected, which will then notify other player? 
		
		menu.enableEverything ();
	}
	
	[RPC]
	void pausedOn()
	{
		//Debug.Log ("Player 2 paused.");
		paused = true;
		boxdisplay.networkView.RPC ("setImage", RPCMode.All, "Paused_small"); 
		boxdisplay.renderer.enabled = true;
		p2script.pauseAnimation (true);
		p2script.enabled = false;
		freezeScript.enabled = false;
		rigidbody2D.velocity = Vector3.zero;
		//showPause ();
	}
	[RPC]
	void pausedOff()
	{
		paused = false;
		boxdisplay.renderer.enabled = false;
		p2script.pauseAnimation (false);
		p2script.enabled = true;
		freezeScript.enabled = true;
		//hidePause ();

	}

	void showPause() {
		inGamePause.renderer.material.color = new Color(1,1,1,1);
	}
	
	void hidePause() {
		inGamePause.renderer.material.color = new Color(1,1,1,0);
	}
}
