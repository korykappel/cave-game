﻿using UnityEngine;
using System.Collections;

public class ScreenTint : MonoBehaviour {

	private bool tintOn;
	private Color tempColor;
	private float freezeLeft;

	public float maxTintAmount;

	// Use this for initialization
	void Start () {
		tintOn = false;
		tempColor = Color.grey;
		freezeLeft = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		if(tintOn)
		{
			tempColor.a = maxTintAmount * freezeLeft;
			renderer.material.SetColor ("_TintColor", tempColor);
		}
		else {
			tempColor.a = 0.0f;
			renderer.material.SetColor ("_TintColor", tempColor);
		}
	}

	public void setTintLocal(bool b)
	{
		tintOn = b;
	}

	public void setFreezeLeftLocal(float f)
	{
		freezeLeft = f;
	}

	[RPC]
	public void setTint(bool b)
	{
		tintOn = b;
	}

	[RPC]
	public void setFreezeLeft(float f)
	{
		freezeLeft = f;
	}
}
