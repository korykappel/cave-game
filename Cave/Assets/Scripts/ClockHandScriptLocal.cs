﻿using UnityEngine;
using System.Collections;

public class ClockHandScriptLocal : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Transform p1 = GameObject.Find ("Player1(Clone)").transform;
		transform.position = p1.position;
		transform.parent = p1;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void setHand(float percentFull)
	{
		Vector3 tmp = transform.eulerAngles;
		tmp.z = 360 * -percentFull;
		transform.eulerAngles = tmp;
	}
}
