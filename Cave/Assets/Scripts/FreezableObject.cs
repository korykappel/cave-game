﻿using UnityEngine;
using System.Collections;

public class FreezableObject : MonoBehaviour {
	
	protected bool frozen;
	protected Vector2 savedVel;
	protected bool unFrozenLastFrame;
	
	// Use this for initialization
	void Start () {
		frozen = false;
		unFrozenLastFrame = false;
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	[RPC]
	public void freeze()
	{
		frozen = true;
		if(rigidbody2D)
			savedVel = rigidbody2D.velocity;
	}

	[RPC]
	public void unfreeze()
	{
		frozen = false;
		unFrozenLastFrame = true;
	}

	public void freezeLocal()
	{
		frozen = true;
		if(rigidbody2D)
			savedVel = rigidbody2D.velocity;
	}

	public void unfreezeLocal()
	{
		frozen = false;
		unFrozenLastFrame = true;
	}

	public virtual void reset() {}
}