﻿using UnityEngine;
using System.Collections;

public class PressurePlateScript : FreezableObject {

	public GameObject door;
	DoorScript dscript;

	bool colliding;

	void Start () {
		dscript = door.gameObject.GetComponent<DoorScript>();
		colliding = false;
	}

	//This runs before both collisions and Update. Sets colliding to false as the default for the frame
	void FixedUpdate()
	{
		colliding = false;
	}

	//runs after collisions. Colliding will only be true if it's colliding this frame
	void Update () {
		if(!frozen)
		{
			if(colliding)//Make button go down and door go up
			{
				//3.4 down to 1.22
				if(transform.localScale.y > 1.22) 
					transform.localScale -= new Vector3(0,.1f,0);
				dscript.up = true;
				dscript.down = false;
			}
			else{ //Make button go up and door go down
				if(transform.localScale.y < 3.4) 
					transform.localScale += new Vector3(0,.1f,0);
				dscript.up = false;
				dscript.down = true;
			}
		}
	}

	//The reason we had to use this method is because while the button was shrinking every frame, it was going in and out of colliding
	//every frame. So using OnCollisionExit was not a good method to use for setting the button to go back up - we have to make sure
	//it's actually not colliding this frame. 
	void OnCollisionEnter2D(Collision2D col) 
	{
		colliding = true;
	}
	void OnCollisionStay2D(Collision2D col)
	{
		colliding = true;
	}
}
