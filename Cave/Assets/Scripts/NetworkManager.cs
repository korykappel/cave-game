﻿/*
How the network manager works:

When player pressed "Online co-op," and tries to start the game,
the game will request a list of games being hosted on the server.

If there are no games on the server:
	A game will be registered on the server with the comment attached: "Level_X" where X 
		is the level the player is starting on (0 if new game, n if on level n)

	Registering this game on the server can sometimes take up to 10 seconds if one has a 
	slow connection, so sometiems if someone registers a server and someone else tries to join 
	immediately, the game won't be set up on the server yet and so both parties will become
	their own servers, waiting for people to join them.
	
	Then the user is told they are waiting for another player to join. Once another 
	player joins, the game starts with both players at the desired level.

If there are games on the server:
	The games' comments will be parsed, looking for a game with the comment, "Level_X" where 
	X is the game that the player is loking for. If there are no games with the desired level,
	or if there are games but they already have two players, then the player becomes a server
	by starting his/her own game and waiting for someone else to join him.


Why this paradigm?
	This would work well if there were lots of people trying to play the game. There are only 6 
	possible levels for people to start on, and with a large user base, there would be many people 
	on all six levels at any given time which should make it fairly easy to join games.

	Also note, that this means the server/client instantiation is essentially "random" for the 
	player, which means the character he plays in game will also be "random." The server is 
	always the red character, the client is always the blue character. This is intentional, as
	the focus isn't about who you are as you descend the cave, but more that it's a story about
	the nature of man and the nature of reality in general, so we didn't see it as a big problem
	to not allow the user to choose which character to be.

	However, since we don't have a large user base, it's really only practical to select "New Game" 
	if one is playing online.

 */

using UnityEngine;
using System.Collections;

public class NetworkManager : MonoBehaviour
{
	private float btnX, btnY, btnW, btnH;
	private string gameTypeName = "edu.gcc.comp477.cave.james";
	private bool refreshingHostList = false;
	private bool hostDataFound = false;
	private HostData[] hostData;
	public GameObject player1;
	public GameObject player2;
	public GameObject door;
	private GameObject waitingOnP2Screen;
	private GameObject sorryScreen;
	private bool useAWSserver = false;
	private string AWS_URL;

	private GameManager manager;
	private int refreshCount = 0;
	private int refreshMax = 100;
	private int currentLevel = 0;
	private bool startServer = false;
	private bool gameFitsTheBill = false;
	private int gameToJoin = -1;
	private bool shouldKeepUpdating = true;
	private bool serverInitialized = false;
	private bool connectedToServer = false;
	private static bool initiatedByClient = false;


	void Start ()
	{
		manager = (GameManager)FindObjectOfType<GameManager> ();
		setupAwsServerIfBeingUsed();
		waitingOnP2Screen = GameObject.FindGameObjectWithTag("waitingOnP2");
		sorryScreen = GameObject.FindGameObjectWithTag("sorryScreen");
		//waitingOnP2Screen.SetActive (false);
		MasterServer.RequestHostList(gameTypeName);
	}
	
	void Update ()
	{
		if (shouldKeepUpdating) {
			if (refreshCount < refreshMax && !hostDataFound) {
				if (MasterServer.PollHostList ().Length > 0) {
					hostData = MasterServer.PollHostList ();
					hostDataFound = true;
					gameToJoin = checkForApplicableGames ();
				}
				refreshCount++;
			}

			if (refreshCount >= refreshMax  || hostDataFound)
				shouldKeepUpdating = false;

			if (shouldKeepUpdating == false) {
				if (hostDataFound) {		Debug.Log ("Games Exist");
					if (gameToJoin != -1) 	startServer = false;
					else 					startServer = true;
				} else { //all refreshes finished
//					Debug.Log ("No games exist.");
					startServer = true;
				}

				if (startServer) {
//					Debug.Log ("Attempting to start own server.");
					Network.InitializeServer (2, 25001, !Network.HavePublicAddress());
					MasterServer.RegisterHost(gameTypeName, "CaveGameBeta", "Level_" + currentLevel.ToString());
				} else {
//					Debug.Log("Attempting to join game " + gameToJoin.ToString());
					Network.Connect(hostData[gameToJoin]);
				}
			}
		}


	}

	int checkForApplicableGames ()
	{
		for (int i = 0; i < hostData.Length; i++)
			if (hostData[i].comment.Contains(currentLevel.ToString()) && hostData[i].connectedPlayers < 2)
				return i;
		return -1;
	}
	
	//Messages
	void OnServerInitialized ()
	{
		Debug.Log ("Server initialized");
	}
	
	void OnConnectedToServer ()
	{
		Debug.Log ("Connected to server");
		waitingOnP2Screen.SetActive (false);
		manager.networkView.RPC("bothPlayersConnected", RPCMode.All, null);
	}

	public static void disconnectServer() {


		if (Network.isClient) 
		{
			initiatedByClient = true;
			Network.CloseConnection(Network.connections[0], true); //I think this code will disconnect from the game
		}
		else if (Network.isServer)
		{
			Network.CloseConnection(Network.connections[0], true);
			Network.Disconnect ();
			MasterServer.UnregisterHost();
		}
	}
	void OnPlayerDisconnected(NetworkPlayer player) {
		Network.RemoveRPCs(player);
		Network.DestroyPlayerObjects (player);
		Network.Disconnect();
		MasterServer.UnregisterHost();
		sorryScreen.renderer.material.color = new Color(1,1,1,1);

		//Player1Control.setWaitingForButton();

		Debug.Log("Player disconnected from server.");
	}
	
	void OnDisconnectedFromServer(NetworkDisconnection info) {
		if (Network.isServer)	
			Debug.Log ("Network Manager: I DONT THINK THIS SHOULD EVER BE SEEN ");
		if (Network.isClient) {
			if (initiatedByClient)  {
				initiatedByClient = false; //reset for next time
				return;
			}
			else {
				sorryScreen.renderer.material.color = new Color(1,1,1,1);
			}
			Debug.Log ("Client Disconnected from server: " + info);
		}
		//	Debug.Log ("Message 4");
		//Network.DestroyPlayerObjects(Network.player);
		//Debug.Log ("Message 5");
		//Network.RemoveRPCs (Network.player);
		//Debug.Log ("Message 6");
	}

	void setupAwsServerIfBeingUsed ()
	{
		//Modify the Master server and facilitator attributes to connect to my locally
		//hosted server instead of the Unity HQ Master Server
		if (useAWSserver) {
			//Use the following four lines if using the class AWS Master Server
			MasterServer.ipAddress = AWS_URL;
			//"54.187.184.133"; //"10.37.101.31";
			MasterServer.port = 23466;
			Network.natFacilitatorIP = AWS_URL;
			//"54.187.184.133"; //"10.37.101.31";
			Network.natFacilitatorPort = 50005;
		}
	}
}


