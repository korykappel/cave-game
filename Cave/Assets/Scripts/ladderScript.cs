﻿using UnityEngine;
using System.Collections;

public class ladderScript : MonoBehaviour {

	private GameManager manager;

	private ControlScript script;
	// Use this for initialization
	void Start () {
		manager = (GameManager)FindObjectOfType (typeof(GameManager));
		script = null;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerStay2D(Collider2D col)
	{
		//if (script == null) {
		//	try {
		if(col.gameObject.name == "Player1(Clone)" || col.gameObject.name == "Player2(Clone)")
		{
			if(col.gameObject.name == "Player1(Clone)")
			{
				if(manager.getNetworked()) script = col.gameObject.GetComponent<Player1Control>();
				else if(manager.getLocal()) script = col.gameObject.GetComponent<Player1ControlLocal>();
			}
			else //player 2 clone
			{
				if(manager.getNetworked()) script = col.gameObject.GetComponent<Player2Control>();
				else if(manager.getLocal()) script = col.gameObject.GetComponent<Player2ControlLocal>();;
			}
			if(!script.getOnLadder() && (Mathf.Abs(script.getVertMove()) > .7) && closeToLadder(col.gameObject))
			{
				script.setOnLadder (true);
				script.setLadderBottom(renderer.bounds.min.y);
				script.setLadderTop(renderer.bounds.max.y);
				col.gameObject.rigidbody2D.velocity = Vector2.zero;
				col.gameObject.rigidbody2D.gravityScale = 0;
				col.gameObject.transform.position = new Vector2(renderer.bounds.center.x, col.gameObject.transform.position.y);
			}
		}
		//	} catch {}
		//}

		//if (script != null) {
			//if not already on ladder and colliding with ladder, close enough to it, and pressing up or down, then "snap" to ladder
			
		//}
	}

	bool closeToLadder(GameObject player)
	{
		float maxDistance = .3f;
		return (Mathf.Abs (renderer.bounds.center.x - player.renderer.bounds.center.x) < maxDistance);
	}
}
