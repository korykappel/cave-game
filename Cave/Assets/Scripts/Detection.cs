﻿using UnityEngine;
using System.Collections;

public class Detection : MonoBehaviour {

	public enum Req {RED, BLACK, BOTH, BOTHHAVEBEEN};
	public enum Action {CAMERATOPLAYERS, CAMERATOLEVEL, GOODNOISE, BADNOISE, NEXTLEVEL, ENTERTRANSITION, EXITTRANSITION, END, END2};
	public Req TriggerRequirements;
	public Action action;

	private int playersInside = 0;
	private bool p1Inside = false;
	private bool p2Inside = false;
	private bool p1WasInside, p2WasInside = false;

	private ToggleableCamera cameraScript;
	private GameObject camera;
	private GameManager manager;

	//used to reset P1's rewindbar
	private RewindScript rewindScript;
	private FreezeScript freezeScript;

	// Use this for initialization
	void Start () {
		cameraScript = (ToggleableCamera)FindObjectOfType (typeof(ToggleableCamera));
		camera  	 = cameraScript.gameObject;
		manager = (GameManager)FindObjectOfType (typeof(GameManager));
	}
	
	void OnTriggerEnter2D(Collider2D other) {
				if (!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
						return;

				if (other.gameObject.tag == "P1") {
						playersInside++;
						p1Inside = p1WasInside = true;
						rewindScript = other.gameObject.GetComponent<RewindScript> ();
				}
				if (other.gameObject.tag == "P2") {
						playersInside++;
						p2Inside = p2WasInside = true;
						freezeScript = other.gameObject.GetComponent<FreezeScript> ();
				}

				if (TriggerRequirements.ToString () == "BOTH") {
						if (p1Inside && p2Inside)
								doAction ();
				} else if (TriggerRequirements.ToString () == "BOTHHAVEBEEN") {
						if (p1WasInside && p2WasInside)
								doAction ();
				} else if (TriggerRequirements.ToString () == "RED") {
						if (p1Inside && !p2Inside) 
								doAction ();
				} else if (TriggerRequirements.ToString () == "BLACK") {
					if (!p1Inside && p2Inside)
							doAction ();
				} 
	}
	
	void OnTriggerExit2D(Collider2D other) {
		if(!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
			return;

		if (other.gameObject.tag == "P1") {
			playersInside--;
			p1Inside = false;
		}
		if (other.gameObject.tag == "P2") {
			playersInside--;
			p2Inside = false;
		}

	}

	void doAction() {
		if(!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
			return;

		//CAMERATOGGLE, GOODNOISE, BADNOISE
		if (action.ToString () == "CAMERATOPLAYERS") {
			try {
				camera.networkView.RPC("chasePlayers", RPCMode.All, null);
			} catch {
				Debug.Log ("Something went wrong");
			}
		} else if (action.ToString () == "CAMERATOLEVEL") {
			try {
				camera.networkView.RPC("chaseLevel", RPCMode.All, null);
			}catch {
				Debug.Log("Something went wrong.");
			}
		}else if (action.ToString () == "GOODNOISE") {
			Debug.Log ("Unimplemented good noise.");
		} else if (action.ToString () == "BADNOISE") {
			Debug.Log ("Unimplemented bad noise.");
		} else if(action.ToString () == "ENTERTRANSITION") {
			manager.loadNextLevel ();
			Debug.Log ("Called");
			//gameObject.SetActive (false);
			gameObject.layer = LayerMask.NameToLayer ("Void");

			string t = gameObject.tag;
			manager.setLevel(int.Parse(t.Substring(t.LastIndexOf('l')+1)));

			rewindScript.networkView.RPC("resetRewind", RPCMode.All, null);
			rewindScript.networkView.RPC("setPowerEnabled", RPCMode.All, false);
			freezeScript.networkView.RPC("disablePower", RPCMode.All, null);
			try {
				camera.networkView.RPC("chasePlayers", RPCMode.All, null);
			} catch {
				Debug.Log ("Camera cannot chase players");
			}
		} else if(action.ToString () == "EXITTRANSITION") {

			//gameObject.SetActive (false);
			gameObject.layer = LayerMask.NameToLayer ("Void");

			int children = transform.childCount;
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).gameObject.layer = LayerMask.NameToLayer ("Void");

			string t = gameObject.tag;
			manager.setLevel(int.Parse(t.Substring(t.LastIndexOf('l')+1)));

			rewindScript.networkView.RPC("resetRewind", RPCMode.All, null);
			rewindScript.networkView.RPC("setPowerEnabled", RPCMode.All, true);
			freezeScript.networkView.RPC("enablePower", RPCMode.All, null);
			freezeScript.networkView.RPC("resetFreeze", RPCMode.All, null);
			try {
				camera.networkView.RPC("chaseLevel", RPCMode.All, null);
			}catch {
				Debug.Log("Camera cannot chase level");
			}
		} else if (action.ToString () == "END") { 
			manager.end();
			int children = transform.childCount;
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).gameObject.layer = LayerMask.NameToLayer ("Void");
		} else if (action.ToString() == "END2") {
			manager.end2();
		} else {
			Debug.Log ("Uninitialized action for this trigger.");
		}
	}

}
