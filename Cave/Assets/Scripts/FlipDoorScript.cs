﻿using UnityEngine;
using System.Collections;

public class FlipDoorScript : FreezableObject {
	
	bool opening, closing;
	
	public enum Direction {SWINGLEFT, SWINGRIGHT};
	public enum Orientation {CLOSED, OPEN};
	public Direction direction;
	public Orientation orientation;
	public float swingSpeed = 100f;
	
	//XORed with opening and closing to take start position into account
	private bool startedOpen;
	
	int angle;
	
	// Use this for initialization
	void Start () {
		rigidbody2D.isKinematic = true;
		
		opening = false;
		closing = true;
		
		if (orientation == Orientation.CLOSED) {
			startedOpen = false;
			Vector3 tmp = transform.eulerAngles;
			tmp.z = 0;
			transform.eulerAngles = tmp;
		}
		else {
			startedOpen = true;			
			Vector3 tmp = transform.eulerAngles;
			
			if (direction.ToString() == "SWINGLEFT") {
				tmp.z = 270;
			}
			else {
				tmp.z = 90;
			}
			transform.eulerAngles = tmp;
			
		}

	}
	
	// Update is called once per frame
	void Update () {
		if (!frozen) {
			if(Network.isServer)
			{
				if(direction.ToString() == "SWINGLEFT")
				{
					if((opening ^ startedOpen) && (transform.rotation.eulerAngles.z > 270 || transform.rotation.eulerAngles.z == 0)) 
					{
						transform.Rotate (new Vector3(0,0,-1f) * (swingSpeed * Time.deltaTime));
						//if(transform.rotation.eulerAngles.z < 360) transform.rotation.eulerAngles.Set (0,0,0);
						if(transform.rotation.eulerAngles.z < 270) 
						{
							Vector3 tmp = transform.eulerAngles;
							tmp.z = 270;
							transform.eulerAngles = tmp;
						}
					}
					else if((closing ^ startedOpen) && transform.rotation.eulerAngles.z > 0) {
						transform.Rotate (new Vector3(0,0,1f) * (swingSpeed * Time.deltaTime));
						if(transform.rotation.eulerAngles.z < 5) 
						{
							Vector3 tmp = transform.eulerAngles;
							tmp.z = 0;
							transform.eulerAngles = tmp;
						}
					}
				}
				else{ //SWINGRIGHT
					if((opening ^ startedOpen) && (transform.rotation.eulerAngles.z < 90)) 
					{
						transform.Rotate (new Vector3(0,0,1f) * (swingSpeed * Time.deltaTime));
						//if(transform.rotation.eulerAngles.z < 360) transform.rotation.eulerAngles.Set (0,0,0);
						if(transform.rotation.eulerAngles.z > 90) 
						{
							Vector3 tmp = transform.eulerAngles;
							tmp.z = 90;
							transform.eulerAngles = tmp;
						}
					}
					else if((closing ^ startedOpen) && transform.rotation.eulerAngles.z > 0) {
						transform.Rotate (new Vector3(0,0,-1f) * (swingSpeed * Time.deltaTime));
						if(transform.rotation.eulerAngles.z > 350) 
						{
							Vector3 tmp = transform.eulerAngles;
							tmp.z = 0;
							transform.eulerAngles = tmp;
						}
					}
				}
			}
		}
		//Debug.Log (transform.rotation.eulerAngles);
		
	}
	
	public void flipSwitch()
	{
		if(opening)
		{
			opening = false;
			closing = true;
		}
		else if(closing)
		{
			opening = true;
			closing = false;
		}
	}

	public override void reset()
	{
		if(Network.isServer)
		{
			opening = false;
			closing = true;
			
			if (orientation == Orientation.CLOSED) {
				startedOpen = false;
				Vector3 tmp = transform.eulerAngles;
				tmp.z = 0;
				transform.eulerAngles = tmp;
			}
			else {
				startedOpen = true;			
				Vector3 tmp = transform.eulerAngles;
				
				if (direction.ToString() == "SWINGLEFT") {
					tmp.z = 270;
				}
				else {
					tmp.z = 90;
				}
				transform.eulerAngles = tmp;	
			}
		}
	}
}
