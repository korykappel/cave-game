﻿using UnityEngine;
using System.Collections;

public class P2BoxDisplay: MonoBehaviour {
	
	SpriteRenderer renderer;
	GameManager manager;

	Vector3 convoPosition = new Vector3(2.1f, 2.5f, 0);
	Vector3 restartPosition = new Vector3(0,1.5f,0);
	Vector3 pausedPosition = new Vector3(0,2.2f,0);
	
	Transform p2;

	// Use this for initialization
	void Start () {
		
		renderer = GetComponent<SpriteRenderer> ();
		renderer.enabled = false;

		manager = (GameManager)FindObjectOfType<GameManager> ();

		//manager needs access to this display box
		manager.getP2BoxDisplay (networkView.viewID);
	}
	
	[RPC]
	void setImage(string image)
	{
		renderer.sprite = Resources.Load<Sprite>("Images/" + image);
		if(image.ToString().Contains("TryingToRestart"))
			transform.position = p2.position + restartPosition;
		else if(image.ToString().Contains("Paused_small"))
			transform.position = p2.position + pausedPosition;
		else if(image.ToString().Contains("convo"))
			transform.position = p2.position + convoPosition;
	}
	
	[RPC]
	void initialize()
	{
		p2 = GameObject.Find ("Player2(Clone)").transform;
		transform.position = p2.position + convoPosition;
		transform.parent = p2;
	}
	
	//IEnumerator designates this function as a coroutine, which can return after one frame and pick up execution in the next
	public IEnumerator displayForXSeconds(float seconds)
	{
		if(Network.isClient)
		{
			networkView.RPC ("setVisible", RPCMode.All, true);
			float timer = 0;
			while(timer < seconds)
			{
				timer += Time.deltaTime;
				yield return null;
			}
			networkView.RPC ("setVisible", RPCMode.All, false);
		}
	}
	
	[RPC]
	void setVisible(bool b)
	{
		renderer.enabled = b;
	}
}
