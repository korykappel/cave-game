﻿using UnityEngine;
using System.Collections;

public class fade : MonoBehaviour {

	float alpha = 1.0f;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI()
	{
		
		alpha = Mathf.Lerp(alpha, 1, 0.1f * Time.deltaTime);
		
		guiTexture.color = new Color(1,1,1, alpha);
	}
}
