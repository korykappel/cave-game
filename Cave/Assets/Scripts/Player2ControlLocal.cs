﻿using UnityEngine;
using System.Collections;

public class Player2ControlLocal : ControlScript {

	
	private RewindScriptLocal p1rewind;
	private GameObject player1;
	private Player1ControlLocal p1script;
	protected ToggleableCameraLocal cameraScript;

	private bool p1Grabbed;

	private Animator p2Animator;
	private bool animMoving;
	private bool animJumping;
	private bool animDirectionRight;

	private bool canJump;

	private float p2Color;
	private SpriteRenderer p2SpriteRenderer;

	private float percentShadowed;
	private bool turningIntoShadows;
	private float shadowficationDuration;
	
	void Start () {
		grabbed 	= false;
		gameStarted = true;
		p1rewind 	= (RewindScriptLocal)FindObjectOfType (typeof(RewindScriptLocal));
		player1  	= p1rewind.gameObject;
		p1script 	= player1.GetComponent<Player1ControlLocal>();
		onLadder = false;
		voidLayer = LayerMask.NameToLayer ("Void");
		

		renderer.sortingOrder = 7;
		cameraScript = (ToggleableCameraLocal)FindObjectOfType (typeof(ToggleableCameraLocal));
		cameraScript.gatherRequiredInformation ();

		p2Animator = this.GetComponent<Animator>();
		//initialize animator vars
		animMoving = false;
		animJumping = false;
		animDirectionRight = true;
		p2Animator.SetBool("Moving", animMoving);
		p2Animator.SetBool("Jumping", animJumping);

		canJump = false;

		p2Color = 0.0f;
		p2SpriteRenderer = this.GetComponent<SpriteRenderer> ();

		percentShadowed = 0.0f;
		turningIntoShadows = false;
		shadowficationDuration = 0.0f;
	}
	
	void Update()
	{
		updateInputs(ref move, ref vertMove, ref jump, ref breakOff, ref tryingToGrab, ref p1Grabbed);
	}

	void OnCollisionStay2D(Collision2D col)
	{
		if(col.gameObject.tag.ToString() == "Jumpable")
		{
			canJump = true;
		}
	}
	
	void FixedUpdate () { //FixedUpdate is good for physics stuff -- has nothing to do with time
		if(gameStarted)
		{
			if (turningIntoShadows) {
				p2Color = 1.0f - 0.95f * percentShadowed;
				p2SpriteRenderer.color = new Vector4 (p2Color, p2Color, p2Color, 1);
			}
			
			if (!grabbed) {
				if(onLadder) handleLadderMovement();
				else handleStandardMovement();
				
				//Grab code
				if(tryingToGrab && !p1Grabbed && close() && !p1rewind.getBarFull())
					p1script.grabPlayerLocal ();
				if(!tryingToGrab && p1Grabbed)
					p1script.dropPlayerLocal (transform.position);
				
			}
			else //grabbed
			{
				transform.position = player1.transform.position;
				//rigidbody2D.velocity = player1.rigidbody2D.velocity;
				rigidbody2D.velocity = Vector2.zero;
				
				if (breakOff)
					p1script.dropPlayerLocal (player1.transform.position);
				
			}

			canJump = false;
		
			updateAnimation ();
		}
	}

	void updateAnimation() {

		int children = transform.childCount;

		if (animDirectionRight) {
			transform.localScale = new Vector3 (1, 1, 1);
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).transform.localScale = new Vector3(2,2,1);
		} else {
			transform.localScale = new Vector3 (-1, 1, 1);
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).transform.localScale = new Vector3(-2,2,1);
		}
		p2Animator.SetBool("Moving", animMoving);
		p2Animator.SetBool("Jumping", animJumping);
	}

	public void pauseAnimation(bool b) {
		p2Animator.enabled=!b;
	}

	void updateInputs (ref float move, ref float vertMove, ref bool jump, ref bool breakOff, ref bool tryingToGrab, ref bool p1Grabbed)
	{
		move = Input.GetAxis ("P2_Horizontal");
		vertMove = Input.GetAxis ("P2_Vertical");
		jump = Input.GetButtonDown ("P2_Jump");
		breakOff = Input.GetButtonDown ("P2_BreakOff");
		tryingToGrab = Input.GetButton("P2_Action");
		p1Grabbed = p1script.getGrabbed();
	}
	
	void handleStandardMovement ()
	{
		Vector2 vel = rigidbody2D.velocity;
		if (Mathf.Abs (move) >= joystickMoveThreshold) {
			vel.x = move * maxSpeed;
			animMoving = true;
			if(vel.x > 0) {
				animDirectionRight = true;
			}
			else {
				animDirectionRight = false;
			}
		}
		else {
			vel.x = 0;
			animMoving = false;
		}
		vel.y = rigidbody2D.velocity.y;
		
		//if (jump && Mathf.Abs(vel.y) <= .6)
		if(jump && canJump) {
			//vel.y += vertSpeed;
			vel.y = vertSpeed;
			animJumping= true;
			animMoving = false;
			//Debug.Log("Jumping");
		}
		else if(vel.y == 0) {
			animJumping = false;
		}
		rigidbody2D.velocity = vel;
	}
	
	void handleLadderMovement()
	{
		Vector2 vel = Vector2.zero;
		if (Mathf.Abs (vertMove) >= joystickMoveThreshold)
			vel.y = -vertMove * maxSpeed;
		
		if(vel.y > 0 && renderer.bounds.max.y >= ladderTop)
			vel.y = 0;
		
		//break off ladder if jump or left or right is pressed (just like Braid :P )
		if (jump || Mathf.Abs(move) > .4) {
			breakFromLadder();
		}
		
		if(vel.y != 0) {
			animMoving=true;
		} 
		else {
			animMoving = false;
		}
		
		rigidbody2D.velocity = vel;
	}
	
	void breakFromLadder()
	{
		onLadder = false;
		rigidbody2D.gravityScale = 1.0f;
	}
	
	//returns true if the players are close enough for "grabbing"
	bool close() {
		int maxDistance = 1;
		return (Mathf.Abs(renderer.bounds.center.x - player1.renderer.bounds.center.x) < maxDistance && Mathf.Abs(renderer.bounds.center.y - player1.renderer.bounds.center.y) < maxDistance);
	}
	
	public bool getGrabbed() {
		return grabbed;
	}
	
	void disableCollider(Collider2D col) {
		col.gameObject.layer = voidLayer;
	}
	
//	void OnGUI() {
//		GUI.Label (new Rect(0,50,500,500), gameObject.layer.ToString());
//	}
	
	void enableCollider(Collider2D col)
	{
		col.gameObject.layer = 0;
	}
	

	void setKinematicLocal(bool b) {
		gameObject.rigidbody2D.isKinematic = b;
	}

	void setCollision(bool b) {
		if (b) {
			enableCollider(gameObject.collider2D);
		} else {
			disableCollider(gameObject.collider2D);
		}
	}

	void setGrabbed(bool g)
	{
		grabbed = g;
	}
	

	public void setVelocityLocal(Vector3 v) {
		rigidbody2D.velocity = v;
	}
	

	public void setPositionLocal(Vector3 v) 
	{
		transform.position = new Vector2(v.x, v.y);
	}
	

	public void dropPlayerLocal(Vector3 pos)
	{
		grabbed = false;
		setPositionLocal (pos);
		setVelocityLocal (new Vector3 (0, 0, 0));
		setKinematicLocal (false);
	}
	

	public void grabPlayerLocal() {
		grabbed = true;
		setKinematicLocal (true);
	}

	public void startShadowfication(float secondsUntilShadowfied){
		shadowficationDuration = secondsUntilShadowfied;
		StartCoroutine("shadowfication");
	}

	IEnumerator shadowfication()
	{
		turningIntoShadows = true;
		float timer = 0.0f;
		while(timer < shadowficationDuration) 
		{
			timer += Time.deltaTime;
			percentShadowed = timer / shadowficationDuration;
			yield return null;
		}
	}
}