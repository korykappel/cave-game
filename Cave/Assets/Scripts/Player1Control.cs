using UnityEngine;
using System.Collections;

public class Player1Control : ControlScript {

	private FreezeScript freezeScript;

	private RewindScript rewindScript;
	//private NetworkView camView;
	private NetworkView p2NetworkView;
	private bool tryingToRewind;
	private bool p2Grabbed;
	private GameObject p2;
	private NetworkView p2view;
	private Player2Control p2script;
	private ClockHandScript clockHandScript;
	private Animator p1Animator;
	private bool animMoving;
	private bool animJumping;
	private bool animDirectionRight;
	private bool canJump;
	private bool tryingToRestart;

	private float p1Color;
	private SpriteRenderer p1SpriteRenderer;
	
	private float percentShadowed;
	private bool turningIntoShadows;
	private float shadowficationDuration;
	private bool p2TryingToRestart = false;

	// Use this for initialization
	void Start () {
		rewindScript = (RewindScript)FindObjectOfType (typeof(RewindScript));
		grabbed 	 = false;
		gameStarted  = false;
		onLadder = false;
		voidLayer = LayerMask.NameToLayer ("Void");
		if (Network.isClient) {
			rewindScript.enabled = false;
			//cameraScript = (ToggleableCamera)FindObjectOfType (typeof(ToggleableCamera));
			//cam = cameraScript.gameObject; 
			//camView = cameraScript.networkView;
			//camView.RPC("initialize", RPCMode.All, null);
		}
		if (Network.isServer)
			renderer.sortingOrder = 8;

		//clockHandScript = (ClockHandScript)FindObjectOfType (typeof(ClockHandScript));
		//clockHandScript.enableClock ();

		p1Animator = this.GetComponent<Animator>();
		// init animation controller vars
		animMoving = false;
		animJumping = false;
		animDirectionRight = true;
		p1Animator.SetBool("Moving", animMoving);
		p1Animator.SetBool("Jumping", animJumping);
		canJump = false;
		tryingToRestart = false;

		p1Color = 0.0f;
		p1SpriteRenderer = this.GetComponent<SpriteRenderer> ();
		
		percentShadowed = 0.0f;
		turningIntoShadows = false;
		shadowficationDuration = 0.0f;
	}

	void disableCollider(Collider2D col) {
		col.gameObject.layer = voidLayer;
	}
	
	void enableCollider(Collider2D col)
	{
		col.gameObject.layer = 0;
	}

	void Update()
	{
		updateInputs(ref tryingToRestart, ref tryingToRewind, ref move, ref vertMove, ref jump, ref tryingToGrab, ref breakOff, ref p2Grabbed);
	}

	void OnCollisionStay2D(Collision2D col)
	{
		if(col.gameObject.tag.ToString() == "Jumpable")
		{
			canJump = true;
		}
	}

	void FixedUpdate () { //FixedUpdate is good for physics stuff -- has nothing to do with time
		if (!(gameStarted && Network.isServer)) { 
			updateAnimation ();
			return;
		}
		

		if (paused) {
			displayPauseMenu();
		}

		if (!turningIntoShadows) {
			p1Color = 1.0f - 0.92f * rewindScript.getPercentFull ();
			p1SpriteRenderer.color = new Vector4 (p1Color, p1Color, p1Color, 1);
		} 
		else {
			p1Color = 1.0f - 0.95f * percentShadowed;
			p1SpriteRenderer.color = new Vector4 (p1Color, p1Color, p1Color, 1);
			networkView.RPC("setPercentShadowed", RPCMode.All, percentShadowed);
		}

		if (!freezeScript.getFrozen() && !rewindScript.getBarFull()) {
			if(rigidbody2D.isKinematic)
				networkView.RPC ("setKinematic",RPCMode.All,false);

			if (!grabbed) {
				if (!(tryingToRewind && rewindScript.getPowerEnabled())) 
				{	
					if(onLadder) handleLadderMovement();
					else handleStandardMovement();
				}

				//Grabbing code
				if (tryingToGrab && !p2Grabbed && close() && !rewindScript.getBarFull()) 
					p2view.RPC("grabPlayer", RPCMode.All, null);
				if (!tryingToGrab && p2Grabbed)
					p2view.RPC("dropPlayer", RPCMode.All, transform.position);

			} else { //grabbed

				transform.position = p2.transform.position;
				rigidbody2D.velocity = Vector2.zero;

				if(breakOff || rewindScript.getBarFull()) {
					if (breakOff) Debug.Log ("Broken off");
					if (tryingToRewind) Debug.Log ("Trying to rewind.");
					if (rewindScript.getBarFull()) Debug.Log ("Rewind Bar full.");
					networkView.RPC ("dropPlayer", RPCMode.All, p2.transform.position);
				}				
			}
		} else { //frozen
			networkView.RPC ("setKinematic",RPCMode.All,true);
			if(grabbed) {
				transform.position = p2.transform.position;
				rigidbody2D.velocity = new Vector2(0,0);
			}

		}

		canJump = false;

		updateAnimation ();
		
	}

	void displayPauseMenu ()
	{
	}

	void updateInputs (ref bool restart, ref bool rewind, ref float move, ref float vertMove, ref bool jump, ref bool tryingToGrab, ref bool breakOff, ref bool p2Grabbed)
	{
		if (!enabled || !Network.isServer) return;

		//restart = Input.GetButton ("P1_Restart");

		restart = Input.GetButton ("P1_Restart");
		rewind = Input.GetButton("Rewind");
		move = Input.GetAxis ("P1_Horizontal");
		vertMove = Input.GetAxis ("P1_Vertical");
		jump = Input.GetButtonDown ("P1_Jump");
		tryingToGrab = Input.GetButton ("P1_Action");
		breakOff = Input.GetButtonDown ("P1_BreakOff");
		p2Grabbed = p2script.getGrabbed();
		paused = Input.GetButtonDown("P1_Pause");
	}

	void updateAnimation() {
		int children = transform.childCount;

		if ((tryingToRewind && rewindScript.getPowerEnabled()) || rewindScript.getBarFull() || freezeScript.getFrozen()) {
			p1Animator.enabled=false;
		} 
		else {
			p1Animator.enabled=true;
			if (animDirectionRight) {
				transform.localScale = new Vector3 (1, 1, 1);
				for (int i = 0; i < children; ++i)
					transform.GetChild(i).transform.localScale = new Vector3(2,2,1);
			}
			else {
				transform.localScale = new Vector3 (-1, 1, 1);
				for (int i = 0; i < children; ++i)
					transform.GetChild(i).transform.localScale = new Vector3(-2,2,1);
			}
			p1Animator.SetBool ("Moving", animMoving);
			p1Animator.SetBool ("Jumping", animJumping);
		}
	}

	public void pauseAnimation(bool b) {
		p1Animator.enabled=!b;
	}

	void handleStandardMovement ()
	{
		if(rewindScript.movementBarNotFull())
		{
			Vector2 vel = rigidbody2D.velocity;
			if (Mathf.Abs (move) >= joystickMoveThreshold) {
				vel.x = move * maxSpeed;
				animMoving = true;
				if(vel.x > 0) {
					animDirectionRight = true;
				}
				else {
					animDirectionRight = false;
				}
			}
			else {
				vel.x = 0;
				animMoving = false;
			}
			vel.y = rigidbody2D.velocity.y;

			//if (jump && Mathf.Abs(vel.y) <= .6)
			if(jump && canJump) {
				//vel.y += vertSpeed;
				vel.y = vertSpeed;
				animJumping= true;
				animMoving = false;
				//Debug.Log("Jumping");
			}
			else if(vel.y == 0) {
				animJumping = false;
			}
			rigidbody2D.velocity = vel;
		}
	}
	
	void handleLadderMovement()
	{
		//if(rewindScript.movementBarNotFull())
		//{
		Vector2 vel = Vector2.zero;
		if (Mathf.Abs (vertMove) >= joystickMoveThreshold)
			vel.y = -vertMove * maxSpeed;
		
		if(vel.y > 0 && renderer.bounds.max.y >= ladderTop)
			vel.y = 0;
		
		//break off ladder if jump or left or right is pressed (just like Braid :P )
		if (jump || Mathf.Abs(move) > .4) {
			breakFromLadder();
		}
		
		if(vel.y != 0) {
			animMoving=true;
		} 
		else {
			animMoving = false;
		}
		
		rigidbody2D.velocity = vel;
		//}
	}
	
	void breakFromLadder()
	{
		onLadder = false;
		rigidbody2D.gravityScale = 1.0f;
	}

	public bool getGrabbed() {
		return grabbed;
	}

	//returns true if the players are close enough for "grabbing"
	bool close() {
		int maxDistance = 1;
		return (Mathf.Abs(renderer.bounds.center.x - p2.renderer.bounds.center.x) < maxDistance && Mathf.Abs(renderer.bounds.center.y - p2.renderer.bounds.center.y) < maxDistance);
	}

	public void startShadowfication(float secondsUntilShadowfied){
		shadowficationDuration = secondsUntilShadowfied;
		StartCoroutine("shadowfication");
	}
	
	IEnumerator shadowfication()
	{
		turningIntoShadows = true;
		float timer = 0.0f;
		while(timer < shadowficationDuration) 
		{
			timer += Time.deltaTime;
			percentShadowed = timer / shadowficationDuration;
			yield return null;
		}
	}

	/********** Stream Serialization *******************/

	void OnSerializeNetworkView(BitStream stream, NetworkMessageInfo info) {
		//fires up when data to be sent.
		Vector3 pos = Vector3.zero;
		Vector3 vel = Vector3.zero;
		bool moving = false;
		bool jumping = false;
		bool dirRight = true;
		float color = 0.0f;
		//bool g = false; //grabbed - dunno if it should be set to false...
		if (stream.isWriting) { //we want to send data
			pos = transform.position;
			vel = rigidbody2D.velocity;
			moving = animMoving;
			jumping = animJumping;
			dirRight = animDirectionRight;
			color = p1Color;
			stream.Serialize(ref pos);
			stream.Serialize(ref vel);
			stream.Serialize(ref moving);
			stream.Serialize(ref jumping);
			stream.Serialize(ref dirRight);
			stream.Serialize(ref color);
		} else { //we deal with received data
			stream.Serialize(ref pos);
			transform.position = pos;
			stream.Serialize(ref vel);
			rigidbody2D.velocity = vel;
			stream.Serialize(ref moving);
			animMoving = moving;
			stream.Serialize(ref jumping);
			animJumping = jumping;
			stream.Serialize(ref dirRight);
			animDirectionRight = dirRight;
			stream.Serialize(ref color);
			p1Color = color;
			p1SpriteRenderer.color = new Vector4(p1Color, p1Color, p1Color, 1);
			//updateAnimation();
		}
	}


	/********** Remote Procedure Calls *******************/

	[RPC]
	void setCollision(bool b) {
		if (b) {
			enableCollider(gameObject.collider2D);
		} else {
			disableCollider(gameObject.collider2D);
		}
	}

	[RPC]
	void getOtherPlayer(NetworkViewID id)
	{
		gameStarted = true;
		p2 = NetworkView.Find(id).gameObject;
		p2NetworkView = p2.networkView;
		p2script = p2.GetComponent<Player2Control> ();
		freezeScript = p2.GetComponent<FreezeScript>();
		p2view = p2.GetComponent<NetworkView>();
	}

	[RPC]
	void setKinematic(bool k)
	{
		rigidbody2D.isKinematic = k;
		if (k)
			p1Animator.enabled = false;
		else
			p1Animator.enabled = true;
	}


	[RPC]
	void grabPlayer() {
		grabbed = true;
		networkView.RPC("setKinematic", RPCMode.All, true);
	}

	[RPC]
	void setVelocity(Vector3 v) {
		rigidbody2D.velocity = new Vector2 (v.x, v.y);
	}

	[RPC]
	void setPosition(Vector3 v) {
		transform.position = v;
	}

	[RPC]
	void p2Restarting() {
		if (tryingToRestart) {
			GameManager.restart = true; 
		}
	}

	[RPC]
	void p2NotRestarting() {
		GameManager.restart = false;
	}

	[RPC]
	void dropPlayer(Vector3 pos)
	{
		grabbed = false;
		networkView.RPC("setPosition", RPCMode.All, pos);
		networkView.RPC("setVelocity", RPCMode.All, new Vector3(0,0,0));
		networkView.RPC("setKinematic", RPCMode.All, false);
	}

	[RPC]
	void setPercentShadowed(float perSh)
	{
		percentShadowed = perSh;
	}
}
