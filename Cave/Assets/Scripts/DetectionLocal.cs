﻿using UnityEngine;
using System.Collections;

public class DetectionLocal : MonoBehaviour {
	
	public enum Req {RED, BLACK, BOTH, BOTHHAVEBEEN};
	public enum Action {CAMERATOPLAYERS, CAMERATOLEVEL, GOODNOISE, BADNOISE, ENTERTRANSITION, EXITTRANSITION, END, END2};
	public Req TriggerRequirements;
	public Action action;
	
	private int playersInside = 0;
	private bool p1Inside = false;
	private bool p2Inside = false;
	private bool p1WasInside, p2WasInside = false;
	
	private ToggleableCameraLocal cameraScript;
	private GameManager manager;

	//used to enable/disable powers
	private RewindScriptLocal rewindScript;
	private FreezeScriptLocal freezeScript;
	
	// Use this for initialization
	void Start () {
		cameraScript = (ToggleableCameraLocal)FindObjectOfType (typeof(ToggleableCameraLocal));
		manager = (GameManager)FindObjectOfType (typeof(GameManager));
	}
	
	void OnTriggerEnter2D(Collider2D other) {

		if(!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
			return;

		if (other.gameObject.tag == "P1") {
			//Debug.Log("p1 in");
			playersInside++;
			p1Inside = p1WasInside = true;
			rewindScript = other.gameObject.GetComponent<RewindScriptLocal>();
			rewindScript.resetRewindLocal();
		}
		else if (other.gameObject.tag == "P2") {
			//Debug.Log("P2 in");
			playersInside++;
			p2Inside = p2WasInside = true;
			freezeScript = other.gameObject.GetComponent<FreezeScriptLocal>();
		}
		
		if (TriggerRequirements.ToString() == "BOTH") {
			if (p1Inside && p2Inside)
				doAction();
		} else if(TriggerRequirements.ToString() == "BOTHHAVEBEEN") {
			if(p1WasInside && p2WasInside)
				doAction();
		} else if (TriggerRequirements.ToString() == "RED") {
			if (p1Inside && !p2Inside) 
				doAction();
		} else if (TriggerRequirements.ToString () == "BLACK") {
			if (!p1Inside && p2Inside)
				doAction ();
		}
	}
	
	void OnTriggerExit2D(Collider2D other) {
		if(!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
			return;

		if (other.gameObject.tag == "P1") {
			playersInside--;
			p1Inside = false;
		}
		else if (other.gameObject.tag == "P2") {
			playersInside--;
			p2Inside = false;
		}
		
	}
	
	void doAction() {
		if(!enabled || !gameObject.activeSelf || !gameObject.activeInHierarchy)
			return;


		//CAMERATOGGLE, GOODNOISE, BADNOISE
		if (action.ToString () == "CAMERATOPLAYERS") {
			try {
				cameraScript.chasePlayersLocal();
			} catch {
				Debug.Log ("Something went wrong");
			}
		} else if (action.ToString () == "CAMERATOLEVEL") {
			try {
				cameraScript.chaseLevelLocal();
			}catch {
				Debug.Log("Something went wrong.");
			}
		}else if (action.ToString () == "GOODNOISE") {
			Debug.Log ("Unimplemented good noise.");
		} else if (action.ToString () == "BADNOISE") {
			Debug.Log ("Unimplemented bad noise.");
		} else if(action.ToString () == "ENTERTRANSITION") {
			//Debug.Log("Working!");
			//manager.loadNextLevel ();

			//gameObject.SetActive (false);
			gameObject.layer = LayerMask.NameToLayer ("Void");

			string t = gameObject.tag;
			manager.setLevel(int.Parse(t.Substring(t.LastIndexOf('l')+1)));

			//rewindScript.resetRewindLocal ();
			//Debug.Log ("powers disabled called.");
			rewindScript.setPowerEnabledLocal(false);
			freezeScript.disablePowerLocal();
			try {
				cameraScript.chasePlayersLocal();
			} catch {
				Debug.Log ("Camera cannot chase players");
			}
		} else if(action.ToString () == "EXITTRANSITION") {
			//gameObject.SetActive (false);
			gameObject.layer = LayerMask.NameToLayer ("Void");
			int children = transform.childCount;
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).gameObject.layer = LayerMask.NameToLayer ("Void");

			string t = gameObject.tag;
			manager.setLevel(int.Parse(t.Substring(t.LastIndexOf('l')+1)));

			rewindScript.setPowerEnabledLocal (true);
			//rewindScript.resetRewindLocal();
			freezeScript.enablePowerLocal();
			freezeScript.resetFreezeLocal();
			try {
				cameraScript.chaseLevelLocal();
				//Debug.Log("function called?");
			}catch {
				Debug.Log("Camera cannot chase level");
			}
		} else if (action.ToString () == "END") {
			manager.end();
			int children = transform.childCount;
			for (int i = 0; i < children; ++i)
				transform.GetChild(i).gameObject.layer = LayerMask.NameToLayer ("Void");
		} else if (action.ToString() == "END2") {
			manager.end2();
		} else {
			Debug.Log ("Uninitialized action for this trigger.");
		}
	}
	
}
