﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SwitchScriptLocal : MonoBehaviour {
	
	private bool switchPressP1, switchPressP2;
	public GameObject[] swingDoors;

	private SpriteRenderer spriteRenderer;

	public Sprite offSwitch;
	public Sprite onSwitch;

	bool isSwitchOn = false;

	private List<FlipDoorScriptLocal> flipDoorScripts = new List<FlipDoorScriptLocal>();


	void Awake()
	{
		spriteRenderer = GetComponent<SpriteRenderer> ();
	}
	// Use this for initialization
	void Start () {
		switchPressP1 = switchPressP2 = false;

		for (int i = 0; i < swingDoors.Length; i++)
			flipDoorScripts.Add(swingDoors[i].GetComponent<FlipDoorScriptLocal>()); 
	}
	
	// Update is called once per frame
	void Update () {
		switchPressP1 = Input.GetButtonDown ("P1_SwitchPress");
		switchPressP2 = Input.GetButtonDown ("P2_SwitchPress");
	}
	
	void OnTriggerStay2D(Collider2D col)
	{
		//if(Network.isServer)
		//{
		if((switchPressP1 && col.gameObject.tag == "P1") || (switchPressP2 && col.gameObject.tag == "P2"))
		{
			//always flips the state of the switch, i.e., true => false or false => true
			isSwitchOn ^= true;

			if(isSwitchOn)
				spriteRenderer.sprite = onSwitch;
			else
				spriteRenderer.sprite = offSwitch;
			
				//swingDoorScript.flipSwitch();
			for (int i = 0; i < swingDoors.Length; i++)
				flipDoorScripts[i].flipSwitch();
			//swingDoorScript.snap();
			//swingDoorScript.networkView.RPC("flipSwitch", RPCMode.All, null);
		}
		//}
	}

	public void reset()
	{
		//isSwitchOn = false;
		spriteRenderer.sprite = offSwitch;
	}
}
