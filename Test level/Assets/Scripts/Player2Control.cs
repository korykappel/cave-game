﻿using UnityEngine;
using System.Collections;

public class Player2Control : MonoBehaviour {
	
	public float maxSpeed = 10f;
	public float vertSpeed = 10f;
	public float joystickMoveThreshold = 1f;

	private Player1Control p1script;
	private GameObject player1;
	
	bool jump;
	float move;
	public bool grabbed;

	// Use this for initialization
	void Start () {
		grabbed = false;
		p1script = (Player1Control)FindObjectOfType (typeof(Player1Control));
		player1 = GameObject.Find ("Player1");
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void FixedUpdate () { //FixedUpdate is good for physics stuff -- has nothing to do with time
		move = Input.GetAxis ("P2_Horizontal");
		//float moveUp = Input.GetAxis ("Vertical");
		jump = Input.GetButtonDown ("P2_Jump");
		//grabbing = Input.GetButton ("P2_Action");
		bool breakOff = Input.GetButtonDown ("P2_BreakOff");

		if (!grabbed) {
			Vector2 vel = rigidbody2D.velocity;
			if(Mathf.Abs(move)>=joystickMoveThreshold) vel.x = move * maxSpeed;
			else vel.x = 0;
			vel.y = rigidbody2D.velocity.y;
			if (jump && vel.y == 0)
					vel.y += vertSpeed;
			rigidbody2D.velocity = vel;
		}
		else
		{
			transform.position = player1.transform.position;
			rigidbody2D.velocity = new Vector2(0,0);
			if(breakOff) {
				grabbed = false;
				//renderer.enabled = true;
			//	Physics2D.IgnoreCollision(collider2D, player1.collider2D, false);
			}
		}
		if(Input.GetButtonDown ("P2_Action") && !grabbed && close ())
		{
			p1script.grabbed = true;
			//Physics2D.IgnoreCollision(collider2D, player1.collider2D);
			//player1.renderer.enabled = false;
		}
		if(!Input.GetButton ("P2_Action") && p1script.grabbed) //they could have broken it off
		{
			p1script.grabbed = false;
			//player1.renderer.enabled = true;
			//Physics2D.IgnoreCollision(collider2D, player1.collider2D, false);
			player1.rigidbody2D.velocity = new Vector2(0,0);
			player1.transform.position = transform.position;
		}
	}

	bool close() {
		return (Mathf.Abs(renderer.bounds.center.x - player1.renderer.bounds.center.x) < 1 && Mathf.Abs(renderer.bounds.center.y - player1.renderer.bounds.center.y) < 1);
	}
}