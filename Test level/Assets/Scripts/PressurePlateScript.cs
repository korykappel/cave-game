﻿using UnityEngine;
using System.Collections;

public class PressurePlateScript : MonoBehaviour {

	public GameObject door;
	DoorScript dscript;

	bool shrink, unshrink;

	// Use this for initialization
	void Start () {
		dscript = door.gameObject.GetComponent<DoorScript>();
		shrink = unshrink = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(shrink) startShrink ();
		else if(unshrink) reverseShrink ();
	}

	void OnCollisionEnter2D(Collision2D col) 
	{
		if(col.gameObject.name == "Player1" || col.gameObject.name == "Player2")
		{
			dscript.up = true;
			dscript.down = false;
			shrink = true;
			unshrink = false;
		}
	}
	void OnCollisionExit2D(Collision2D col)
	{
		if(col.gameObject.name == "Player1" || col.gameObject.name == "Player2")
		{
			dscript.down = true;
			dscript.up = false;
			unshrink = true;
			shrink = false;
		}
	}

	//3.4 down to 1.22
	void startShrink()
	{
		if(transform.localScale.y > 1.22) 
			transform.localScale -= new Vector3(0,.1f,0);
	}
	void reverseShrink()
	{
		if(transform.localScale.y < 3.4) 
			transform.localScale += new Vector3(0,.1f,0);
	}
}
