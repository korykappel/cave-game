﻿using UnityEngine;
using System.Collections;

public class Player1Control : MonoBehaviour {

	public float maxSpeed = 10f;
	public float vertSpeed = 10f;

	private FreezeScript freezeScript;
	private RewindScript rewindScript;
	private GameObject player2;
	private Player2Control p2script;
	private float joystickMoveThreshhold = 1f;
	bool jump;
	float move;
	bool rewind;
	public bool grabbed;


	// Use this for initialization
	void Start () {
		freezeScript = (FreezeScript)FindObjectOfType (typeof(FreezeScript));
		rewindScript = (RewindScript)FindObjectOfType (typeof(RewindScript));
		p2script = (Player2Control)FindObjectOfType (typeof(Player2Control));
		player2 = GameObject.Find ("Player2");
		grabbed = false;
	}
	
	// Update is called once per frame
	void Update () {
		rewind = Input.GetButton("Rewind");
	}

	void FixedUpdate () { //FixedUpdate is good for physics stuff -- has nothing to do with time
		move = Input.GetAxis ("P1_Horizontal");
		//float moveUp = Input.GetAxis ("Vertical");
		jump = Input.GetButtonDown ("P1_Jump");
		//grabbing = Input.GetButton ("P1_Action");
		//bool grabButtonDown = Input.GetButtonDown ("P1_Action");
		bool breakOff = Input.GetButtonDown ("P1_BreakOff");

		//regular movement, if not being grabbed, and no one is using their power
		if (!grabbed && !rewind && !freezeScript.frozen) {
			Vector2 vel = rigidbody2D.velocity;
			if(Mathf.Abs(move)>=joystickMoveThreshhold) vel.x = move * maxSpeed;
			else vel.x = 0;
			vel.y = rigidbody2D.velocity.y;
			if (jump && vel.y == 0)
				vel.y += vertSpeed;
			rigidbody2D.velocity = vel;
		}

		//if time is frozen, he's kinematic, else if time's not frozen and his bar isn't full, he's not kinematic
		if (freezeScript.frozen) {
			if(!grabbed) rigidbody2D.isKinematic = true;
		} else if(!rewindScript.barFull){
			rigidbody2D.isKinematic = false;
		} 
		//player no longer "grabbed" once rewind bar gets full
		if (rewindScript.barFull) {
			if(grabbed) grabbed = false;
			//if(!p2script.grabbed) Physics2D.IgnoreCollision(collider2D, player2.collider2D, false);
		}

		//grabs other player if close and not being grabbed himself
		if(Input.GetButtonDown ("P1_Action") && !grabbed && close ())
		{
			p2script.grabbed = true;
			//player2.renderer.enabled = false;
			//Physics2D.IgnoreCollision(collider2D, player2.collider2D);
		}

		//drop player when grab button no longer held...? 
		if(!Input.GetButton ("P1_Action") && p2script.grabbed) //they could have broken it off
		{
			p2script.grabbed = false;
			//player2.renderer.enabled = true;
			//Physics2D.IgnoreCollision(collider2D, player2.collider2D, false);
			player2.transform.position = transform.position;
			player2.rigidbody2D.velocity = new Vector2(0,0);
		}

		if(grabbed)
		{
			transform.position = player2.transform.position;
			rigidbody2D.velocity = new Vector2(0,0);
			if(breakOff) {
				grabbed = false;
				//renderer.enabled = true;
				//Physics2D.IgnoreCollision(collider2D, player2.collider2D, false);
				//transform.position = player2.transform.position;
			}
		}
	}


	bool close() {
		return (Mathf.Abs(renderer.bounds.center.x - player2.renderer.bounds.center.x) < 1 && Mathf.Abs(renderer.bounds.center.y - player2.renderer.bounds.center.y) < 1);
	}

	void OnGUI() {
			//GUI.Label (new Rect (0, 50, 500, 500), "Player1 grabbed: " + grabbed.ToString () + "  Player2 grabbed: " + p2script.grabbed.ToString());
		}
}
