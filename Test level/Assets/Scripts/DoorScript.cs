﻿using UnityEngine;
using System.Collections;

public class DoorScript : FreezableObject {

	public bool up, down;
	public float framesToTakeUp, framesToTakeDown;
	//int framesGone;
	float distanceGone;
	float distanceToTravel = .9f;
	float distancePerFrameUp, distancePerFrameDown;

	// Use this for initialization
	void Start () {
		//framesGone = 0;
		distanceGone = 0;
		up = down = false;
		distancePerFrameUp = distanceToTravel / framesToTakeUp;
		distancePerFrameDown = distanceToTravel / framesToTakeDown;
	}
	
	// Update is called once per frame
	void Update () {
		if (!frozen) {
			if (up && distanceGone < distanceToTravel) {
					distanceGone += distancePerFrameUp;
					transform.position = new Vector2 (transform.position.x, transform.position.y + distancePerFrameUp);
			} else if (up && distanceGone >= distanceToTravel) {
					//framesGone = 0;
					up = false;
			}
			if (down && distanceGone > 0) {
					distanceGone -= distancePerFrameDown;
					transform.position = new Vector2 (transform.position.x, transform.position.y - distancePerFrameDown);
			} else if (down && distanceGone <= 0) {
					//framesGone = 0;
					down = false;
			}
		} else {

		}
	}
}
